#!/bin/bash
# Preparatory file laying basis for a workflow copied in parallel across as
#	many as feasible nodes of a supercomputing system organised in the style of
#	those of the Texas Advanced Computing Center 
# Workflow itself actually contained in "upconvert.sh"


set	-x


set	-e
	unset		CDPATH
# Act upon the target (TGT) across the threads of a given number of nodes


# SBATCH options
#OUTPUT="$HOME/.slurm"
	JOBNAME='mf-upconversion'
	PARTITION='normal'
#PARTITION='debug'
	TIME='04:00:00'
#TIME='48:00:00'
	ACCOUNT='Groundwater-Decision'
# Maximum permissible number of nodes to request
	NNODEMAX=8
# Name of parallelised script
	PARSHPRE='upconvert'
# Prefix under which related is named *-fncs.sh file
	TGTSPRE="nams"
# Archive for completed targets
	TGTDIR="$DATA/nams"
# Checking the existence of necessary shell commands:
# 	- for POSIX-compliance,	use 'command -v'
# 	- for BASH-preference,	use 'hash'
	export		EXISTS='command -v'
# Preferred TMP directory
	export		TEMPDIR="$WORK/tmp/$UID"


# Ensuring proper execution throughout
	unalias	-a


	dirsource													()	{
# Find location of this very script
		SOURCE="${BASH_SOURCE[0]}"
		if															\
				$EXISTS	dirname		>/dev/null						\
			&&	$EXISTS	pwd			>/dev/null						\
			&&	$EXISTS	readlink	>/dev/null						\
			&&	$EXISTS	echo		>/dev/null						;	then
			while	[ -h "$SOURCE"									]	;	do
				ret="$( cd	-P	"$( dirname	"$SOURCE"	)"	&& pwd				)"
				SOURCE="$( readlink	"$SOURCE"									)"
				[[ $SOURCE	!= /*	]]	&& SOURCE="$ret/$SOURCE"
																			done
			ret="$( cd	-P	"$( dirname	"$SOURCE"	)"	&& pwd				)"
			echo	$ret
		fi
																	}
# Location found
	export	DIR=` dirsource											`

# Unclutter functions, which are:
#   - nthread
#   - findtgts
	source		"${DIR}/workflow-fncs.sh"

# Number of threads available
	export	NTHREAD=$(( ` nthread								\
		'/proc/cpuinfo' '^processor'							`	))


#
if															\
		$EXISTS	mkdir		>/dev/null						\
	&&	$EXISTS	find		>/dev/null						\
	&&	$EXISTS	wc			>/dev/null						\
	&&	$EXISTS	rm			>/dev/null						\
	&&	$EXISTS	cat			>/dev/null						\
	&&	$EXISTS	echo		>/dev/null						\
	&&	$EXISTS	chmod		>/dev/null						\
	&&	$EXISTS	cp			>/dev/null						\
	&&	$EXISTS	sed			>/dev/null						\
	&&	$EXISTS	dirname		>/dev/null						\
	&&	$EXISTS basename	>/dev/null						\
	&&	$EXISTS	sbatch		>/dev/null						;	then
		mkdir	-p	"$TEMPDIR"
		TGTS="${TEMPDIR}/${TGTSPRE}.log"
	set	+e
	find	.	-type d -name "* *" -exec sh -c 'mv "$0" "${0// /_}"' {} ';'
	set	-e
		findtgts
		NTGT=$(( ` wc -l	< $TGTS									`	))
# Number of nodes wanted
		NNODEWANT=$(( ( $NTGT	- 1	) / $NTHREAD	+ 1					))
# Number of nodes determined as lesser of wanted versus max
		NNODE=$(( NNODEMAX	< NNODEWANT	? NNODEMAX	: NNODEWANT			))
	
		rm		-f	"${TEMPDIR}/${TGTSPRE}.sh"
		mkdir	-p	"$TGTDIR"
		cat			"$TGTS"	| while IFS= read -r pathname			;	do
			echo			"${PARSHPRE}.sh ${pathname}"					\
				>>	${TEMPDIR}/${TGTSPRE}.sh
																		done
		chmod	+x		"${TEMPDIR}/${TGTSPRE}.sh"
		cp		"${DIR}/${PARSHPRE}.sbatch"	"${TEMPDIR}/${PARSHPRE}.sbatch"
		sed		-ie	"s|\\\\\\\${JOBNAME}|${JOBNAME}|"		${TEMPDIR}/${PARSHPRE}.sbatch
		sed		-ie	"s|\\\\\\\${PARTITION}|${PARTITION}|"	${TEMPDIR}/${PARSHPRE}.sbatch
		sed		-ie	"s|\\\\\\\${NNODE}|${NNODE}|"			${TEMPDIR}/${PARSHPRE}.sbatch
		sed		-ie	"s|\\\\\\\${NTHREAD}|${NTHREAD}|"		${TEMPDIR}/${PARSHPRE}.sbatch
		sed		-ie	"s|\\\\\\\${TIME}|${TIME}|"				${TEMPDIR}/${PARSHPRE}.sbatch
		sed		-ie	"s|\\\\\\\${ACCOUNT}|${ACCOUNT}|"		${TEMPDIR}/${PARSHPRE}.sbatch
		sed		-ie	"s|\\\\\\\${TEMPDIR}|${TEMPDIR}|"		${TEMPDIR}/${PARSHPRE}.sbatch
		sed		-ie	"s|\\\\\\\${TGTSPRE}|${TGTSPRE}|"		${TEMPDIR}/${PARSHPRE}.sbatch
		sbatch	${TEMPDIR}/${PARSHPRE}.sbatch

else

	( >&2 echo	"Error: missing commands"								)

fi


# Reach proper exit
	exit		0
