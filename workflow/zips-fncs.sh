
findtgts													()	{
	find		$PWD	-type f									\
		'(' -iname "*.zip*" -o -iname "*.exe" ')'				\
		> $TGTS														;
																}

writetmplt													()	{
	read	-r -d	''	PARAM	<<- EOM
		dirnam="$1/$2"												&&
		pathnam="$3"												&&
		mkdir	-p							"${TEMPDIR}"			&&
		unzip	-- -o -d "\${dirnam}"		"\${pathnam}"			;
		cp		-a --parents				"\${pathnam}"		\\
			"${TGTDIR}"												;
		rm		-f							"\${pathnam}"			&&
		while	[ "\` find	"\${dirnam}"						\\
			-type f '(' -iname '*.zip*' -o -iname '*.exe' ')'	\\
		| wc	-l	\`"	-gt 0								]	;	do
			find	"\${dirnam}"									\\
				-type f '(' -iname "*.zip*" -o -iname "*.exe" ')'	\\
			| while	read filename									;	do
				dir=\$(		dirname		"\${filename}"					)	&&
				base=\$(	basename	"\${filename}"					)	&&
				name=\${base%.*}											&&
				unzip	-- -o 											\\
					-d "\${dir}/\${name}"	"\${filename}"					;
				cp		-a --parents		"\${filename}"	"${TGTDIR}"		;
				rm		-f					"\${filename}"					;
			done														;
		done														;
	EOM
																}

