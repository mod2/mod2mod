
nthread														()	{
# Gauge number of threads available
	if	$EXISTS	grep	>/dev/null								;	then
		ret=$(( ` grep	-c	$2 $1									`	))
	elif														\
			$EXISTS	cat		>/dev/null							\
		&&	$EXISTS	awk		>/dev/null							\
		&&	$EXISTS	tail	>/dev/null							;	then
		ret=$(( ` cat			$1									\
			| awk		'/'"$2"'/{print $3}'						\
			| tail	-1											`	\
			+	1														))
	else
		( >&2 echo	"Error: missing commands, GReP or Cat and AWK"		)
	fi
	echo	$ret
																}

findtgts													()	{
	find			$PWD	-type f -iname "*.nam"	> ${TGTS}		;
																}

