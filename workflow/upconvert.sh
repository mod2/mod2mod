#!/bin/bash
# Brunt of upconversion workflow amongst versions of MODFLOW
# Improvements to be implemented by order of importance:
#    - Detection of previous upconversion attempt (through use of this
#		workflow): unique file to be placed within each dealt directory
#    - Detection of version of model


set -x


set -e
	pathnam="$1"
	dirnam=$(	dirname		"$pathnam"						)
	basenam=$(	basename	"$pathnam"						)
	filenam=${basenam%.*}
	dirmf96="${dirnam}-mf96-${filenam}"
	dirmf00="${dirnam}-mf00-${filenam}"
	dirmf05="${dirnam}-mf05-${filenam}"
	chmod	700	"${dirnam}"
	find		${dirnam}	-type f -exec chmod 600 {}		\;
	cp		-r	"${dirnam}"	"${dirmf96}"
	cp		-r	"${dirnam}"	"${dirmf00}"
	cd			"${dirmf96}"
	echo		"${basenam}"	> "modflow.bf"
set +e
	modflw96												&
set -e
	cd			"${dirmf00}"
	perl	-e	'for(@ARGV){rename$_,uc}'	*
	pthnam=` find	-type f -iname "*.nam"					`
	drnam=$( dirname	"${pthnam}"							)
	bsnam=$( basename	"${pthnam}"							)
	flnam=${bsnam%.*}
	flnamnam=${flnam}.nam
set	+e
	if	grep	-i	^hfb	"${pthnam}"					;	then
		mf96to2k-hfb		${bsnam}	${flnam}
	else
		mf96to2k-nohfb		${bsnam}	${flnam}
	fi
set	-e
	perl	-e	'for(@ARGV){rename$_,uc}'	*
	tr			'[:lower:]'		'[:upper:]'	< ${bsnam}	\
		> tmp.txt
	mv			tmp.txt			${bsnam}
	echo		"${bsnam}"		> modflow.bf
	cp		-r	"${dirmf00}"	"${dirmf05}"
set +e
	mf2k	${bsnam}										&
set -e
	cd			"${dirmf05}"
	mv			"${bsnam}"		"${flnamnam}"
	flunit6=`cat ${flnamnam} | awk '$2 == "6"'`
	if	[ ${#flunit6}	>	0						]	;	then
		units=( $( cat "${flnamnam}" | awk '{print $2}'	)	)
		array_contains										()	{
			local	array="$1[@]"
			local	seeking=$2
			local	in=1
			for		element	in "${!array}"						;	do
				if	[[ $element	== $seeking						]]	;	then
					in=0
					break
				fi
																	done
			return	$in
																}
		export	LACKS=0
		j=7
		while	true										;	do
			if array_contains	units "$j"						;	then
				j=$[ j	+ 1									]
			else
				sed -ie "s/\(\s\)6\(\s\)/\1$j\2/" ${flnamnam}
				break
			fi
																done
	fi
set	+e
	mf2ktomf05uc	"${flnamnam}"
	mf2005			"${flnamnam}"								&
	wait
	
