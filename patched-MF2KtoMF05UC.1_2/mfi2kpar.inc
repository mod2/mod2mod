C  Data definitions for Named Parameters
      PARAMETER (MXPAR=5000,MXCLST=5000,MXZON=100,MXMLT=100,
     1               MXPERPAR=5000)
      COMMON /GLOCOM/B(MXPAR),B2(MXPAR),ISFILE(MXPAR),ISENS(MXPAR),
     1               LN(MXPAR),BL(MXPAR),BU(MXPAR),IPSUM,ICLSUM,IDEFPAR,
     2               IPLOC(2,MXPAR),IACTIVE(MXPAR),IPCLST(12,MXCLST),
     3               IPPTR(MXPAR),IPERPARPER(MXPERPAR),BSCAL(MXPAR),
     4               BSTART(MXPAR),
     5               ISTRAIN(MXPAR),VALLOW(MXPAR),VALUP(MXPAR)
      COMMON /GLOCOMC/ZONNAM(MXZON),MLTNAM(MXMLT),PARNAM(MXPAR),
     1       PARTYP(MXPAR),MLTFNM(MXMLT),MLTFCN(MXMLT),CPCLST(2,MXCLST),
     2       PERPARNAM(MXPERPAR),PERPARTYP(MXPERPAR)
      CHARACTER*10 ZONNAM,MLTNAM,PARNAM,MLTFNM,CPCLST,PERPARNAM
      CHARACTER*200 MLTFCN
      CHARACTER*4  PARTYP,PERPARTYP
C
C  IPSUM -- Number of parameters that have been defined
C  ICLSUM -- Number of clusters that have been defined
C  IDEFPAR -- Flag that is changed from 0 to 1 when parameter definition
C             is complete
C
C  "p" indicates a parameter number
C  IPLOC(n,p)     n=1 -- first cluster or list location
C                 n=2 -- last cluster or list location
C  IACTIVE(p) -- flag indicating if parameter is active in the current time step
C                            
C  B(p) -- Parameter value
C  PARNAM(p) -- Parameter name
C  PARTYP(p) -- Parameter type
C
C  "c" indicates a cluster
C  IPCLST(n,c) --      n=1 layer #
C                      n=2 multiplier array number (0 indicates none)
C                      n=3 zone array number (0 indicates all cells)
C                      n=4 index of last zone number for this cluster
C                      n=5-14 zone numbers
C
C  IPPTR(p) -- Pointer to parameter number for parameters which are to 
C              undergo estimation or sensitivity analysis, that is, 
C              parameters for which IESTFLG is greater than zero.
C
C  "z" indicates the zone array number
C  IZON(NCOL,NROW,z) -- Zone array
C  ZONNAM(z) -- Zone array name
C
C  "m" indicates the multiplier array number
C  RMLT(NCOL,NROW,m) -- Multiplier array
C  MLTNAM(m) -- multiplier array name
