README.TXT

                      MF2KtoMF05UC - Version: 1.2
Program to Convert MODFLOW-2000 Files to MODFLOW-2005 and UCODE_2005 Files


NOTE: Any use of trade, product or firm names is for descriptive purposes 
      only and does not imply endorsement by the U.S. Government.

This version of MF2KtoMF05UC is packaged for personal computers using
one of the Microsoft Windows operating systems.  An executable file for
personal computers is provided as well as the source code.  The executable
file was created using the Lahey 95 compiler. The source code can be
compiled to run on other computers, but the USGS cannot assist with the
recompiling.


DISTRIBUTION FILE

The following self-extracting distribution file is for use on personal
computers:

         mf2ktomf05uc1_2.exe

The distribution file is a self-extracting program.  Execution of the
distribution file creates numerous individual files.  The extraction
program allows you to specify the directory in which the files should
be restored.  The installation instructions assume that the files are
restored into directory C:\WRDAPP.  The files are then put into
subdirectory mf2ktomf05uc.1_2 . It is recommended that no user files
are kept in the mf2ktomf05uc.1_2 directory.

The files in the distribution are:

   Executable file for personal computers: mf2ktomf05uc.exe

   Documentation:
      Published report -- OFR2007-1204.pdf
      Release history -- release.txt

   Source code:
      mf2ktomf05uc.for
      ospread.for
      mfi2k.inc
      mfi2kpar.inc
      ucode.inc

   Test data: 17 files beginning with "tc1.".
