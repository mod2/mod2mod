      PARAMETER (MXPRIOR=200)
      COMMON /UCODECOM/
     1   Verbose,
     2   ModelLengthUnits,ModelMassUnits,ModelTimeUnits,Sensitivities,
     3   Optimize,Linearity,Prediction,LinearityAdv,EigenValues,
     4   StartRes,Intermedres,FinalRes,StartSens,IntermedSens,FinalSens,
     5   TolPar,TolSOSC,MaxIter,MaxChange,MaxChangerealm,
     6   MqrtFactor,MqrtDirection,MqrtIncrement,
     7   QuasiNewton,QNiter,QNsosr,
     8   PriorInfoValue(MXPRIOR),PVStatistic(MXPRIOR),
     9   PVStatFlag(MXPRIOR),PVPlotSymbol(MXPRIOR),
     @   MinimumSensRatio,ReincludeSensRatio,MaxStep,
     @   OmitInsensitive,TrustRegion,ConsecMax,
     @   SOSsurface,SOSIncrement,CreateInitFiles
      INTEGER Verbose,ModelLengthUnits,ModelMassUnits,ModelTimeUnits,
     1   Sensitivities,Optimize,Linearity,Prediction,LinearityAdv,
     2   EigenValues,StartRes,Intermedres,FinalRes,StartSens,
     3   IntermedSens,FinalSens,MaxIter,MaxChangerealm,
     4   QuasiNewton,QNiter,PVStatFlag,PVPlotSymbol,
     5   OmitInsensitive,TrustRegion,ConsecMax,
     6   SOSsurface,SOSIncrement,CreateInitFiles
      REAL TolPar,TolSOSC,MaxChange,MqrtFactor,MqrtDirection,
     1   MqrtIncrement,QNsosr,PVStatistic,PriorInfoValue,
     2   MinimumSensRatio,ReincludeSensRatio,MaxStep
C
      COMMON /CUCODECOM/ PriorName(MXPRIOR),Equation(MXPRIOR)
      CHARACTER*10 PriorName
      CHARACTER*100 Equation
C
