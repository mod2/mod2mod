C  Global COMMON definition for mfi
      PARAMETER (IUCASE=0)
      PARAMETER (MXSCRN=47,MXVAR=2000)
      PARAMETER (MXFNAM=300,MXAVAI=90)
      PARAMETER (IMISS=-9876,RMISS=-9.876E30)
      PARAMETER (MXCZON=100,MXCOB=100)
      PARAMETER (MXHOB=20000,MXGFOB=5000,MXTFOB=5000,MXCFOB=100000)
      COMMON /GLOBE/MESSFL,NDSNAM,ISUM,ISTRT,IAPART,NLAY,NROW,NCOL,
     1     INBAS,IUNIT(40),IOUT,ILOUT,IGOUT,IUMISC,NMISS,NPER,
     2     MODOUT,MODINP,
     3     NVAR,VARVAL(MXVAR),IVARDA(3,MXVAR),
     5     IUREAL(MXFNAM),IFNUSE(MXFNAM),
     6     IFILSTAT(MXFNAM),IFILREW(MXFNAM),MXUNIT,IUAVAI(MXAVAI),
     7     IHEDUN,IDDNUN,IBOUUN,IBUDUN,IUNIT2(40),
     8     IUHOBSV,IUDROBSV,IURVOBSV,IUGBOBSV,IUCHOBSV
C
      COMMON /GLBCHR/DSNAME,COMENT(20,41),
     1               VARNAM(MXVAR),
     3               FILNAM(MXFNAM),FILTYP(MXFNAM),
     4               CHEDFM,CDDNFM,CBOUFM,
     5               CTEXT(45)
      CHARACTER      DSNAME*28,COMENT*80,
     1               VARNAM*10,
     2               ARRFMT*20,
     3               FILNAM*80,FILTYP*18,
     4               CHEDFM*20,CDDNFM*20,CBOUFM*20,
     5               CTEXT*78
C
C Parameters that may require adjusting depending on the memory available
C on a specific computer:
C  IUCASE -- upper case flag -- 0=upper and lower case file names are
C         allowed; 1=uppercase file names only
C  LENX -- Length of the X array, which is used to store most model data.
C  MXVAR -- maximum number of parameters that can be stored.
C  MXFNAM -- Maximum number of file names
C
C Do not change the following parameters:
C  MXAVAI -- Maximum number of file units that can be allocated (MXUNIT must
C         always be less than or equal to MXAVAI).
C  MXSCRN -- maximum lines that can be added to an AIDE display screen.
C         Assuming that 3 lines are used for "canned text", this leaves
C         47 lines out of the maximum of 50 allowed by AIDE.
C  IMISS -- integer that indicates a value is missing
C  RMISS -- real number that indicates a value is missing.
C  MXCZON -- maximum number of constant-head zones of conscentration
C
C
C Definition of data in COMMON blocks:
C
C  ISUM -- Location in X array of next unused element
C  X -- array that holds most model data
C  MESSFL -- file unit used for AIDE WDM file
C  NDSNAM -- Number of characters in the dataset name, DSNAME
C  DSNAME -- Dataset base name
C  NMISS -- Counter for the number of used elements in the CTEXT array
C  CTEXT(45) -- Buffer for text messages that will eventually be displayed
C  IOUT -- real unit used for log file
C  IUMISC -- real unit that can be used for any I/O needed by a subroutine
C  MODOUT -- Flag for type of model output -- 0 for original MODFLOW format.
C         Not 0 for new free format output.
C  MODINP -- Flag for type of model input -- 0 for original fixed format.
C         Not 0 for free format.
C
C  ISTRT -- MODFLOW's ISTRT flag
C  IAPART -- MODFLOW's IAPART flag
C  NLAY -- Number of layers in model grid
C  NROW -- Number of roes in model grid
C  NCOL -- Number of columns in model grid
C  INBAS -- virtual unit used for BAS package
C  IUNIT -- virtual units for packages -- elements have the same correspondence
C         to packages as for MODFLOW -- element 40 is for MODPATH
C  ILOUT -- virtual unit for MODFLOW listing file
C  IGOUT -- virtual unit for MODFLOW global listing file
C  NPER -- Number of stress periods
C  ISS -- Steady-state flag -- 0 indicates transient
C  COMENT -- Comments
C  CHEDFM -- Format for saving head in an unformatted file
C  CDDNFM -- FOrmat for saving drawdown in an unformatted file
C
C  FILNAM(I) -- File name of virtual unit I.
C  FILTYP(I) -- File type of virtual unit I.
C  IUREAL(I) -- real unit number for virtual unit I.
C  IFNUSE(I) -- usage code for virtual unit I.
C          0 -- unused file (exception: IFNUSE is 0 for the listing file)
C         >0 -- virtual unit of the package file for the package that
C               makes use of this file
C         -1 -- unspecified use -- include in name file
C  MXUNIT -- maximum number of real file units that can be used (this
C         value is different for MODFLOW and MODPATH).
C  IUAVAI -- list of available real file units -- values must be between
C           1 and MXFNAM because these will be put into the name file and
C           can therefore become virtual files when the data are read as
C           old input by mfi.
C
C  IHEDUN -- virtual unit for saving head
C  IDDNUN -- virtual unit for saving drawdown
C  IBUDUN -- tells where cbc budget units are defined:
C                    > 0 indicates all budget terms go to this virtual unit
C                    = 0 indicates units for cbc budget are defined in IUNIT2
C  IUNIT2 -- cbc budget units corresponding to IUNIT values when IBUDUN=0
C             0 indicates no budget output
C             -1 indicates print cbc values
C
C  NVAR -- Number of parameter names that have been defined
C  VARNAM(MXVAR) -- Names of parameters
C  VARVAL(MXVAR) -- Value for each parameter
C  IVARDA(1,N) --  Layer number for parameter (0 if not layer related)
C  IVARDA(2,N) --  Stress period for parameter (0 if not period related)
C  IVARDA(3,N) --  Parameter type -- 0 for constant
C                                   >0 -- array number
