      PROGRAM  MF2KTOMF05UC
C  *********************************************************************
C     5-2-2007   Arlen W. Harbaugh, U.S. Geological Survey
C     This program is used to convert MODFLOW-2000 data to MODFLOW-2005
C  *********************************************************************
      INCLUDE 'mfi2k.inc'
      INCLUDE 'mfi2kpar.inc'
C     ------------------------------------------------------------------
C
C     + + + LOCAL VARIABLES + + +
      DIMENSION IOFLG(200,5)
      DIMENSION NTSOUT(7),INONE(7),IEND(7),IALPER(7),IALSTP(7)
      CHARACTER*6 CCPER
      CHARACTER*80 LINE(1)
      LOGICAL EX,EXMP
      CHARACTER*128 PATNAM,DIRNAM
      CHARACTER*4 CUNIT(40)
      CHARACTER*3 CEXTN(40)
      DATA CUNIT/'BCF6','WEL ','DRN ','RIV ','EVT ','    ','GHB ',
     1           'RCH ','SIP ','DE4 ','SOR ','OC  ','PCG ','LMG ',
     2           'GWT ','    ','    ','    ','    ','    ','    ',
     3           '    ','LPF ','DIS ','SEN ','PES ','OBS ','HOB ',
     4           'ADV ','COB ','ZONE','MULT','DROB','RVOB','GBOB',
     5           'STOB','    ','CHOB','    ','MP  '/
      DATA CEXTN/'BC6' ,'WEL' ,'DRN' ,'RIV' ,'EVT' ,'TLK' ,'GHB' ,
     1           'RCH' ,'SIP' ,'DE4' ,'SOR' ,'OC ' ,'PCG' ,'LMG' ,
     2           'GWT' ,'FHB' ,'RES' ,'STR' ,'IBS' ,'CHD' ,'HF6' ,
     3           '   ' ,'LPF' ,'DIS' ,'SEN' ,'PES' ,'OBS' ,'OBH' ,
     4           'ADV' ,'COB' ,'ZON' ,'MLT' ,'OBD' ,'OBR' ,'OBG' ,
     5           'OST' ,'   ' ,'OBC' ,'   ' ,'MP ' /
C
      COMMON /CSHOBCOM/SHOBNAM
      CHARACTER*12 SHOBNAM(MXHOB)
      COMMON /HOBCOM/LAYHOB(10,MXHOB),IROWHOB(MXHOB),ICOLHOB(MXHOB),
     1               ROFF(MXHOB),COFF(MXHOB),HOBSTAT(MXHOB),HOBS(MXHOB),
     2               IHOBST(MXHOB),IREFSP(MXHOB),HOBTOFF(MXHOB),
     3               IHOBPLT(MXHOB),ITT(MXHOB),DDSTAT(MXHOB),
     4               HOBPR(10,MXHOB)
      COMMON /CHOBCOM/HOBNAM
      CHARACTER*12 HOBNAM(MXHOB),HNAM
C
      COMMON /FOBCOM/LAYFOB(MXCFOB),IROWFOB(MXCFOB),ICOLFOB(MXCFOB),
     1               FOBFACT(MXCFOB),FOBSTAT(MXTFOB),FOBS(MXTFOB),
     2               IFOBST(MXTFOB),IFOBRSP(MXTFOB),FOBTOFF(MXTFOB),
     3               IFOBPLT(MXTFOB),NGFOB,NTFOB,NCFOB,NFOB(6,MXGFOB),
     4               FOBCON(2,40)
      COMMON /CFOBCOM/FOBNAM
      CHARACTER*12 FOBNAM(MXTFOB)      
      COMMON /PESCOM/PRM(200),PRMSP(200),IPRMSF(200),IPRMPS(200)
      COMMON /CPESCOM/PARNEG(200),EQNAM(200),PESEQ(200)
      CHARACTER*10 PARNEG,EQNAM
      CHARACTER*100 PESEQ
      CHARACTER*10 VERSION
C     + + + END SPECIFICATIONS + + +
C
      VERSION='1.2'
      MODINP=0
      MODOUT=0
      ISTRT=0
      IAPART=0
      ISUM=1
      NVAR=0
      NGFOB=0
      NTFOB=0
      NCFOB=0
C
C  All packages are off
      INBAS=0
      ILOUT=0
      IGOUT=0
      IHEDUN=0
      IDDNUN=0
      IBOUUN=0
      IBUDUN=0
      DO 5 I=1,40
      IUNIT(I)=0
      IUNIT2(I)=0
5     CONTINUE
C
C  Define file units for use by MFI for log file and general use
      IOUT=98
      IUMISC=IOUT-1
C
C  Initialize files
      DO 7 I=1,MXFNAM
      IFNUSE(I)=0
      FILTYP(I)=' '
      FILNAM(I)=' '
      IFILSTAT(I)=1
      IFILREW(I)=1
7     CONTINUE
C
      DO 8 I=1,200
      EQNAM(I)=' '
      PESEQ(I)=' '
      PRM(I)=1.0
      PRMSP(I)=0.0
      IPRMSF(I)=1
      IPRMPS(I)=0
      PARNEG(I)=' '
8     CONTINUE
C
C  Initialize observation names
      DO 10 I=1,MXHOB
      SHOBNAM(I)=' '
      HOBNAM(I)=' '
10    CONTINUE
      DO 11 I=1,MXTFOB
      FOBNAM(I)=' '
11    CONTINUE
C
C  Initialize names of parameters.
      DO 30 N=1,MXPAR
        PARNAM(N)=' '
        PARTYP(N)=' '
        IPLOC(1,N)=0
        IPLOC(2,N)=0
        IACTIVE(N)=0
   30 CONTINUE
C
C  Initialize comments
      DO 32 I=1,41
      DO 32 J=1,20
      COMENT(J,I)=' '
32    CONTINUE
C
C  Initialize real and available file units
      CALL MFIUMK
C
C  Get the dataset name
      DSNAME=' '
      CALL GETARG(1,DSNAME)
C      CALL GETCL(DSNAME)
C
C  Get a dataset name if none was specified
46    IF(DSNAME.EQ.' ') THEN
         WRITE(*,*) 'Enter a dataset name'
         READ(*,'(A)') DSNAME
      ENDIF
C
C  Cleanup DSNAME -- remove ".NAM" if it exists
      NDSNAM=LEN_TRIM(DSNAME)
      IF(IUCASE.NE.0) CALL USTRUC(DSNAME,28)
      IF(NDSNAM.GT.4) THEN
         IF(DSNAME(NDSNAM-3:NDSNAM).EQ.'.NAM' .OR.
     1               DSNAME(NDSNAM-3:NDSNAM).EQ.'.nam') THEN
            DSNAME(NDSNAM-3:NDSNAM)=' '
            NDSNAM=NDSNAM-4
         END IF
      END IF
C
C  Open log file and print some information about array sizes
      OPEN(UNIT=IOUT,FILE=DSNAME(1:NDSNAM)//'.log')
      WRITE(IOUT,*) ' M2Kto2005 converter version ',VERSION
      WRITE(IOUT,*)
      WRITE(IOUT,*) ' MODOUT:',MODOUT
      WRITE(IOUT,*) ' Maximum number of variables:',MXVAR
      WRITE(IOUT,*) ' Maximum number of file names:',MXFNAM
      WRITE(IOUT,*)
C
C  Check for an existing dataset
      INQUIRE(FILE=DSNAME(1:NDSNAM)//'.nam',EXIST=EX)
C
      INUNIT=IUMISC-1
      IF(EX) THEN
C  There is an old dataset, so process it
         OPEN(UNIT=IUMISC,FILE=DSNAME(1:NDSNAM)//'.nam',STATUS='OLD')
C  Initialize parameter definition variables.
         IPSUM=0
         ICLSUM=0
         IDEFPAR=0
C
C  Read the MODFLOW data files that will be converted.
         WRITE(*,*) 'Reading old files...'
         CALL SGLOOPEN(IUMISC,CUNIT)
         CALL GLOBALDF(IUNIT(24))
         IF(IUNIT(25).GT.0) CALL SENIN()
         IF(IUNIT(26).GT.0) CALL PESIN()
         IF(IUNIT(27).GT.0) CALL OBSIN()
         IF(IUNIT(28).GT.0) CALL HOBIN()
         IF(IUNIT(33).GT.0) CALL FOBIN(33,'DROB')
         IF(IUNIT(34).GT.0) CALL FOBIN(34,'RVOB')
         IF(IUNIT(35).GT.0) CALL FOBIN(35,'GBOB')
         IF(IUNIT(38).GT.0) CALL FOBIN(38,'CHOB')
C  Make all observation names unique
         CALL DUPOBS
      ELSE
C  Dataset does not exist -- tell user and abort
         WRITE(*,*) 'Dataset does not exist'
         STOP
      END IF
C
C  WRITE MODFLOW-2005 files
      WRITE(*,*) 'Writing New Files...'
      CALL MF2005NAMEFILE()
      IF(IUNIT(28).GT.0) CALL HOBSV()
      IF(IUNIT(33).GT.0) CALL FOBSV(33,'.obd')
      IF(IUNIT(34).GT.0) CALL FOBSV(34,'.obr')
      IF(IUNIT(35).GT.0) CALL FOBSV(35,'.obg')
      IF(IUNIT(38).GT.0) CALL FOBSV(38,'.obc')
      IF(IUNIT(25).GT.0) CALL PVALSV()
      OPEN(UNIT=IUMISC,FILE=DSNAME(1:NDSNAM)//'.bat')
      WRITE(IUMISC,*) 'mf2005 '//DSNAME(1:NDSNAM)
      WRITE(IUMISC,*) 'PAUSE'
      CLOSE(UNIT=IUMISC)
C
C  WRITE UCODE FILES
      IF(IUNIT(25).GT.0 .OR. IUNIT(26).GT.0) THEN
        CALL UCODENAMEFILE
        CALL UCODECONTROLFILE()
        CALL UCODEPARFILE()
        CALL UCODEOBSFILE()
        CALL UCODESV()
        OPEN(UNIT=IUMISC,FILE=DSNAME(1:NDSNAM)//'_ucode.bat')
        WRITE(IUMISC,*) 'ucode_2005 '//DSNAME(1:NDSNAM)//'_ucode.in '
     1                             //DSNAME(1:NDSNAM)//'_ucode'
        WRITE(IUMISC,*) 'PAUSE'
        CLOSE(UNIT=IUMISC)
      END IF
C
      WRITE(*,*) 'Done'
      STOP
      END
      SUBROUTINE SGLOOPEN(INUNIT,CUNIT)
C
C     ******************************************************************
C     OPEN FILES.
C     ******************************************************************
C
C        SPECIFICATIONS:
C     ------------------------------------------------------------------
      INCLUDE 'mfi2k.inc'
      CHARACTER*4 CUNIT(40)
      CHARACTER*80 LINE
      CHARACTER*11 FMTARG
      CHARACTER*18 FT
C     ---------------------------------------------------------------
C
C2------READ A LINE; IGNORE BLANK LINES.
10    READ(INUNIT,'(A)',END=1000) LINE
      IF(LINE.EQ.' ') GO TO 10
      IF(LINE(1:1).EQ.'#') THEN
         WRITE(IOUT,'(A)') LINE
         GO TO 10
      END IF
      IOPN=0
C
C3------DECODE THE FILE TYPE AND UNIT NUMBER.
      LLOC=1
      CALL URWORD(LINE,LLOC,ITYP1,ITYP2,1,N,R,IOUT,INUNIT)
      FT=LINE(ITYP1:ITYP2)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IU,R,IOUT,INUNIT)
      IF(IU.LE.0 .OR. IU.GT.MXFNAM) THEN
         WRITE(IOUT,*) ' File unit out of range:',IU
         CALL RDABRT
      END IF
C
C4------CHECK FOR A VALID FILE TYPE.
      FMTARG='FORMATTED'
      IDATA=0
C
C4A-----CHECK FOR FILE-TYPES "LIST" AND "GLOBAL"
      IF(FT.EQ.'GLOBAL') THEN
         IF(IGOUT.NE.0) THEN
            WRITE(IOUT,*) ' Duplicate definition of GLOBAL file'
            CALL RDABRT
         END IF
         IGOUT=IU
         IFNUSE(IU)=IU
C
      ELSE IF(FT.EQ.'LIST') THEN
         IF(ILOUT.NE.0) THEN
            WRITE(IOUT,*) ' Duplicate definition of LIST file'
            CALL RDABRT
         END IF
         ILOUT=IU
         IFNUSE(IU)=IU
C
C4B-----CHECK FOR "BAS" FILE TYPE.
      ELSE IF(FT.EQ.'BAS6') THEN
         IF(INBAS.NE.0) THEN
            WRITE(IOUT,*) ' Duplicate BAS6 Package file type'
            CALL RDABRT
         END IF
         INBAS=IU
         IOPN=1
         IFNUSE(IU)=IU
C
C4C-----CHECK FOR DATA FILES
      ELSE IF(FT.EQ.'DATA(BINARY)') THEN
         FMTARG='UNFORMATTED'
         IFNUSE(IU)=-1
         IDATA=1
C
      ELSE IF(FT.EQ.'DATA') THEN
         FMTARG='FORMATTED'
         IFNUSE(IU)=-1
         IDATA=1
C
      ELSE IF(FT.EQ.'DATAGLO') THEN
         FT='DATA'
         IFILREW(IU)=2
         FMTARG='FORMATTED'
         IFNUSE(IU)=-1
         IDATA=1
C
      ELSE IF(FT.EQ.'DATAGLO(BINARY)') THEN
         FT='DATA(BINARY)'
         IFILREW(IU)=2
         FMTARG='UNFORMATTED'
         IFNUSE(IU)=-1
         IDATA=1
C
C4D-----CHECK FOR MAJOR OPTIONS.
      ELSE
         DO 20 I=1,40
            IF(FT.EQ.CUNIT(I)) THEN
               IF(IUNIT(I).NE.0) THEN
                  WRITE(IOUT,*) ' Duplicate package file type:',CUNIT(I)
                  CALL RDABRT
               END IF
               IUNIT(I)=IU
               IOPN=1
               IFNUSE(IU)=IU
               GO TO 30
            END IF
20       CONTINUE
         IFNUSE(IU)=-1
30       CONTINUE
      END IF
C
C5------DETERMINE FILE NAME
      IF(IUCASE.EQ.0) THEN
         CALL URWORD(LINE,LLOC,INAM1,INAM2,0,N,R,IOUT,INUNIT)
      ELSE
         CALL URWORD(LINE,LLOC,INAM1,INAM2,1,N,R,IOUT,INUNIT)
      END IF
      WRITE(IOUT,36) LINE(INAM1:INAM2),LINE(ITYP1:ITYP2),IU
36    FORMAT(1X,/1X,'OPENING ',A,/1X,'FILE TYPE:',A,'   UNIT',I4)
C  Don't really open the file.
C
C6------PUT FILE NAME INTO TABLE OF FILE NAMES.
      IF(FILTYP(IU).NE.' ') THEN
         WRITE(IOUT,*) ' ATTEMPT TO OPEN A FILE ON A PREVIOUSLY',
     1        ' DEFINED UNIT'
         WRITE(IOUT,*) ' THE CONFLICT IS ON UNIT',IU
         CALL RDABRT
      END IF
      DO 40 I=1,MXFNAM
      IF(FILNAM(I).EQ.LINE(INAM1:INAM2)) THEN
         WRITE(IOUT,*) ' Duplicate file name:'
         WRITE(IOUT,*) FILNAM(I)
         CALL RDABRT
      END IF
40    CONTINUE
      FILTYP(IU)=FT
      FILNAM(IU)=LINE(INAM1:INAM2)
C  Look for 'OLD'and 'REPLACE' in DATA files
      IF(IDATA.EQ.1) THEN
         CALL URWORD(LINE,LLOC,IST1,IST2,1,N,R,IOUT,INUNIT)
         IF(LINE(IST1:IST2).EQ.'OLD') IFILSTAT(IU)=2
         IF(LINE(IST1:IST2).EQ.'REPLACE') IFILSTAT(IU)=3
      END IF
      GO TO 10
C
C7------END OF NAME FILE.
1000  WRITE(IOUT,*) ' MODINP:',MODINP
      CLOSE(UNIT=INUNIT)
      RETURN
C
C  Open error
2000  WRITE(IOUT,*) 'UNABLE TO OPEN FILE ',LINE(INAM1:INAM2)
      CALL RDABRT
C
      END
      SUBROUTINE HOBSV()
C     ******************************************************************
C     Save HOB data -- MF-2005 version
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      COMMON /SHOBCOM/LAYSHOB(10,MXHOB),IROWSHOB(MXHOB),ICOLSHOB(MXHOB),
     1               SHROFF(MXHOB),SHCOFF(MXHOB),SHOBSTAT(MXHOB),
     2               SHOBS(MXHOB),ISHOBST(MXHOB),ISHREFSP(MXHOB),
     3               SHOBTOFF(MXHOB),ISHOBPLT(MXHOB),SHOBPR(10,MXHOB)
      COMMON /CSHOBCOM/SHOBNAM
      CHARACTER*12 SHOBNAM(MXHOB)
      COMMON /HOBCOM/LAYHOB(10,MXHOB),IROWHOB(MXHOB),ICOLHOB(MXHOB),
     1               ROFF(MXHOB),COFF(MXHOB),HOBSTAT(MXHOB),HOBS(MXHOB),
     2               IHOBST(MXHOB),IREFSP(MXHOB),HOBTOFF(MXHOB),
     3               IHOBPLT(MXHOB),ITT(MXHOB),DDSTAT(MXHOB),
     4               HOBPR(10,MXHOB)
      COMMON /CHOBCOM/HOBNAM
      CHARACTER*12 HOBNAM(MXHOB)      
C     ------------------------------------------------------------------
      IULOC=28
      IFIL=IUMISC
      OPEN(FILE=DSNAME(1:NDSNAM)//'.obh',UNIT=IFIL)
C
C  Write comments
      DO 8 I=1,20
      IF(COMENT(I,IULOC)(1:1).EQ.'#') THEN
         DO 6 N=80,1,-1
         IF(COMENT(I,IULOC)(N:N) .NE. ' ') GO TO 7
6        CONTINUE
7        WRITE(IFIL,'(A)') COMENT(I,IULOC)(1:N)
      END IF
8     CONTINUE
C
C  Count observations
      NH=0
      MOBS=0
      MAXM=0
C
      DO 10 I=1,MXHOB
C  Single time
      IF(SHOBNAM(I).NE.' ') THEN
         NH=NH+1
         NL=0
         DO 9 J=1,10
         IF(LAYSHOB(J,I).GT.0) NL=NL+1
9        CONTINUE
         IF(NL.GT.MAXM) MAXM=NL
         IF(NL.GT.1) MOBS=MOBS+1
      END IF
C  Multi-Time
      IF(HOBNAM(I).NE.' ') THEN
         IF(ITT(I).NE.3) THEN
            NLM=0
            DO 11 J=1,10
            IF(LAYHOB(J,I).GT.0) NLM=NLM+1
11          CONTINUE
            IF(NLM.GT.MAXM) MAXM=NLM
         END IF
         IF(ITT(I).EQ.3) THEN
            NH=NH+1
            IF(NLM.GT.1) MOBS=MOBS+1
            IREFSP(IHEAD)=IREFSP(IHEAD)-1
         ELSE
            IHEAD=I
            IREFSP(IHEAD)=0
         END IF
      END IF
10    CONTINUE
C
      CALL VALGET('TOMULTH',0,0,ITYPE,TOMULTH)
      IF(ITYPE.NE.0) TOMULTH=1.
      CALL VALGET('HOBDRY',0,0,ITYPE,HOBDRY)
      IF(ITYPE.NE.0) HOBDRY=1.E30
C
C  Write items 1 and 2
      IF(MOBS.EQ.0) MAXM=0
      IUX=IUNIT(27)
      WRITE(IFIL,105) NH,MOBS,MAXM,IUX,HOBDRY
105   FORMAT(4I10,1PE13.5,' NH,MOBS,MAXM,IUHOBSV,HOBDRY')
      WRITE(IFIL,106) TOMULTH
106   FORMAT(1P,E13.5,'  TOMULTH')
C
C  Write Single-Time Observations
      DO 200 I=1,MXHOB
      IF(SHOBNAM(I).NE.' ') THEN
         NL=0
         DO 150 J=1,10
         IF(LAYSHOB(J,I).GT.0) NL=NL+1
150      CONTINUE
C  Item 3
         IF(NL.EQ.1) THEN
            WRITE(IFIL,107) SHOBNAM(I),LAYSHOB(1,I),IROWSHOB(I),
     1            ICOLSHOB(I),ISHREFSP(I),SHOBTOFF(I),SHROFF(I),
     2            SHCOFF(I),SHOBS(I)
107         FORMAT(A,4I5,1P,4E14.6)
         ELSE
            WRITE(IFIL,107) SHOBNAM(I),-NL,IROWSHOB(I),
     1            ICOLSHOB(I),ISHREFSP(I),SHOBTOFF(I),SHROFF(I),
     2            SHCOFF(I),SHOBS(I)
C  Item 4
            WRITE(IFIL,'(1P,10(I5,E13.5))')
     1          (LAYSHOB(J,I),SHOBPR(J,I),J=1,NL)
         END IF
      END IF
200   CONTINUE
C
C  Write Multi-Time Observations
      DO 300 I=1,MXHOB
      IF(HOBNAM(I).NE.' ') THEN
        IF(ITT(I).LT.3) THEN
           IDDN=ITT(I)
           IBASE=I+1
           NL=0
           DO 250 J=1,10
           IF(LAYHOB(J,I).GT.0) NL=NL+1
250        CONTINUE
           IF(NL.EQ.1) THEN
             WRITE(IFIL,107) HOBNAM(I),LAYHOB(1,I),IROWHOB(I),
     1            ICOLHOB(I),IREFSP(I),HOBTOFF(I),ROFF(I),COFF(I),
     2            HOBS(I)
           ELSE
             WRITE(IFIL,107) HOBNAM(I),-NL,IROWHOB(I),ICOLHOB(I),
     1            IREFSP(I),HOBTOFF(I),ROFF(I),COFF(I),HOBS(I)
C  Item 4
             WRITE(IFIL,'(1P,10(I5,E13.5))')
     1          (LAYHOB(J,I),HOBPR(J,I),J=1,NL)
           END IF
C  Item 5
           IF(IREFSP(I).LT.0) WRITE(IFIL,'(I5)') ITT(I)
        ELSE
C  Item 6
           IF(IDDN.EQ.2 .AND. I.NE.IBASE) THEN
             WRITE(IFIL,258) HOBNAM(I),IREFSP(I),HOBTOFF(I),HOBS(I)
           ELSE
             WRITE(IFIL,258) HOBNAM(I),IREFSP(I),HOBTOFF(I),HOBS(I)
258          FORMAT(A,I5,1P,2E13.5)
           END IF
        END IF
      END IF
300   CONTINUE
C
      CLOSE(UNIT=IFIL)
C
      RETURN
      END
      SUBROUTINE FOBSV(IULOC,CFEXT)
C     ******************************************************************
C     Save Flow Observations -- MF2005 version
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      COMMON /FOBCOM/LAYFOB(MXCFOB),IROWFOB(MXCFOB),ICOLFOB(MXCFOB),
     1               FOBFACT(MXCFOB),FOBSTAT(MXTFOB),FOBS(MXTFOB),
     2               IFOBST(MXTFOB),IFOBRSP(MXTFOB),FOBTOFF(MXTFOB),
     3               IFOBPLT(MXTFOB),NGFOB,NTFOB,NCFOB,NFOB(6,MXGFOB),
     4               FOBCON(2,40)
      COMMON /CFOBCOM/FOBNAM
      CHARACTER*12 FOBNAM(MXTFOB)      
      CHARACTER*(*) CFEXT
      CHARACTER*200 LINE
C     ------------------------------------------------------------------
C
      IFIL=IUMISC
      OPEN(FILE=DSNAME(1:NDSNAM)//CFEXT,UNIT=IFIL)
C
C  Write comments
      DO 8 I=1,20
      IF(COMENT(I,IULOC)(1:1).EQ.'#') THEN
         DO 6 N=80,1,-1
         IF(COMENT(I,IULOC)(N:N) .NE. ' ') GO TO 7
6        CONTINUE
7        WRITE(IFIL,'(A)') COMENT(I,IULOC)(1:N)
      END IF
8     CONTINUE
C
C  Count NQ, NQC, and NQT
      NQ=0
      NQC=0
      NQT=0
      DO 10 I=1,NGFOB
      IF(NFOB(1,I).EQ.IULOC) THEN
         NQ=NQ+1
         NQC=NQC+ NFOB(5,I)-NFOB(4,I)+1
         NQT=NQT+ NFOB(3,I)-NFOB(2,I)+1
      END IF
10    CONTINUE
C
C  Item 1
      ISV=IUNIT(27)
      WRITE(IFIL,'(4I5,A)') NQ,NQC,NQT,ISV,' NQxx,NQCxx,NQTxx,IUxxOBSV'
C
C  Item 2:
      TOMULT=FOBCON(1,IULOC)
      EVF=FOBCON(2,IULOC)
      WRITE(IFIL,'(1P,E13.5,A)') TOMULT,'  TOMULTxx'
C
C  Item 3
      DO 100 I=1,NGFOB
      IF(NFOB(1,I).EQ.IULOC) THEN
         NQOB=NFOB(3,I)-NFOB(2,I)+1
         NQCL=NFOB(5,I)-NFOB(4,I)+1
         IF(NFOB(6,I).EQ.1) NQCL=-NQCL
         WRITE(IFIL,'(2I10,A)') NQOB,NQCL,'  NQOBxx,NQCLxx'
C  Item 4
         DO 60 J=NFOB(2,I),NFOB(3,I)
         WRITE(IFIL,'(A,I5,1P,2E14.6,A)')
     1             FOBNAM(J),IFOBRSP(J),FOBTOFF(J),FOBS(J),
     2   '  OBSNAM,IREFSP,TOFFSET,HOBS'
60       CONTINUE
C  Item 5
         DO 80 J=NFOB(4,I),NFOB(5,I)
         WRITE(IFIL,'(3I10,1P,E13.5,A)') LAYFOB(J),IROWFOB(J),
     1       ICOLFOB(J),FOBFACT(J),'  LAYER,ROW,COLUMN,FACTOR'
80       CONTINUE
C
      END IF
100   CONTINUE
C
      CLOSE(UNIT=IFIL)
      RETURN
      END
      SUBROUTINE PVALSV()
C     ******************************************************************
C     Save PVAL data
C     ******************************************************************
C
      INCLUDE 'mfi2k.inc'
      INCLUDE 'mfi2kpar.inc'
C     ------------------------------------------------------------------
C
      IFIL=IUMISC
      IULOC=25
      OPEN(FILE=DSNAME(1:NDSNAM)//'.pvl',UNIT=IFIL)
C
C  Write comments
      DO 8 I=1,20
      IF(COMENT(I,IULOC)(1:1).EQ.'#') THEN
         DO 6 N=80,1,-1
         IF(COMENT(I,IULOC)(N:N) .NE. ' ') GO TO 7
6        CONTINUE
7        WRITE(IFIL,'(A)') COMENT(I,IULOC)(1:N)
      END IF
8     CONTINUE
C
C  Count Number of parameters that will be written into SEN file
      NPLIST=0
      NSENS=0
      IF(IPSUM.GT.0) THEN
         DO 100 I=1,IPSUM
         IF(ISFILE(I).NE.0) THEN
            NPLIST=NPLIST+1
            IF(ISENS(I).GT.0) NSENS=NSENS+1
         END IF
100      CONTINUE
      END IF
C
C  Write data
      WRITE(IFIL,105) NPLIST
105   FORMAT(4I10,'  NPLIST')
      IF(NPLIST.GT.0) THEN
         DO 200 I=1,IPSUM
         IF(ISFILE(I).NE.0) THEN
            WRITE(IFIL,107) PARNAM(I),B2(I)
107         FORMAT(A,1X,1P,5E13.5,'  PARNAM,B')
         END IF
200      CONTINUE
      END IF
      CLOSE(UNIT=IFIL)
C
      RETURN
      END
      SUBROUTINE MF2005NAMEFILE()
C     ******************************************************************
C     Copy old name file to dsname_mf2k.nam
C     WRITE MODFLOW-2005 NAME FILE
C     Change GLOBAL file to LIST file
C     Don't include LIST file if there was a GLOBAL file
C     Change DATAGLO to DATA
C     Change SEN file type to PVAL, and change the name to dsname.pvl
C     Change OBS file type, which is IUNIT(27), to DATA with name:
C             "DSNAME//'._OS'" -- for saving simulated equivalents
C     Change the names of all the OBS files to use the new extensions
C     Remove the PES file type
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      CHARACTER*80 LINE
C
C  Copy old Name File
      OPEN(UNIT=80,FILE=DSNAME(1:NDSNAM)//'.nam')
      OPEN(UNIT=81,FILE=DSNAME(1:NDSNAM)//'_mf2k.nam')
      WRITE(81,'(A)') '# MODFLOW-2000 Name File'
5     READ(80,'(A)',END=7) LINE
C  Skip any "# MODFLOW-200" comments
      IF(LINE(1:13).EQ.'# MODFLOW-200') GO TO 5
      N=LEN_TRIM(LINE)
      IF(N.GT.0) THEN
        WRITE(81,'(A)') LINE(1:N)
      ELSE
        WRITE(81,'(A)')
      END IF
      GO TO 5
C
7     CLOSE(UNIT=80)
      CLOSE(UNIT=81)
C
C  Create new name file
      OPEN(UNIT=80,FILE=DSNAME(1:NDSNAM)//'_mf2k.nam')
      OPEN(UNIT=81,FILE=DSNAME(1:NDSNAM)//'.nam')
      WRITE(81,'(A)') '# MODFLOW-2005 Name File'
      ILIST=0
C
10    READ(80,'(A)',END=100) LINE
C  Skip the "# MODFLOW-2000 Name File" comment
      IF(LINE(1:13).EQ.'# MODFLOW-200') GO TO 10
      LLOC=1
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,1,IDUM,RDUM,IOUT,80)
      IF(LINE(ISTART:ISTOP).EQ. 'GLOBAL') THEN
        CALL URWORD(LINE,LLOC,I1,I2,2,IU,RDUM,IOUT,80)
        WRITE(81,'(A,I10,A)') 'LIST',IU,' '//DSNAME(1:NDSNAM)//'.lst'
        ILIST=1
C
      ELSE IF(LINE(ISTART:ISTOP).EQ. 'LIST') THEN
        IF(ILIST.EQ.0) WRITE(81,'(A)') TRIM(LINE)
C
      ELSE IF(LINE(ISTART:ISTOP).EQ. 'SEN') THEN
        CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IU,RDUM,IOUT,80)
        WRITE(81,'(A,I10,A)') 'PVAL',IU,' '//DSNAME(1:NDSNAM)//'.pvl'
C
      ELSE IF(LINE(ISTART:ISTOP).EQ. 'OBS') THEN
        CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IU,RDUM,IOUT,80)
        WRITE(81,'(A,I10,A)') 'DATA',IU,' '//DSNAME(1:NDSNAM)//'._os'
C
      ELSE IF(LINE(ISTART:ISTOP).EQ. 'HOB') THEN
        CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IU,RDUM,IOUT,80)
        WRITE(81,'(A,I11,A)') 'HOB',IU,' '//DSNAME(1:NDSNAM)//'.obh'
C
      ELSE IF(LINE(ISTART:ISTOP).EQ. 'GBOB') THEN
        CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IU,RDUM,IOUT,80)
        WRITE(81,'(A,I10,A)') 'GBOB',IU,' '//DSNAME(1:NDSNAM)//'.obg'
C
      ELSE IF(LINE(ISTART:ISTOP).EQ. 'DROB') THEN
        CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IU,RDUM,IOUT,80)
        WRITE(81,'(A,I10,A)') 'DROB',IU,' '//DSNAME(1:NDSNAM)//'.obd'
C
      ELSE IF(LINE(ISTART:ISTOP).EQ. 'RVOB') THEN
        CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IU,RDUM,IOUT,80)
        WRITE(81,'(A,I10,A)') 'RVOB',IU,' '//DSNAME(1:NDSNAM)//'.obr'
C
      ELSE IF(LINE(ISTART:ISTOP).EQ. 'CHOB') THEN
        CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IU,RDUM,IOUT,80)
        WRITE(81,'(A,I10,A)') 'CHOB',IU,' '//DSNAME(1:NDSNAM)//'.obc'
C
      ELSE IF(LINE(ISTART:ISTOP).EQ. 'PES') THEN
        GO TO 10
C
      ELSE IF(LINE(ISTART:ISTOP).EQ.'PVAL') THEN
         CALL URWORD(LINE,LLOC,ISTART,ISTOP,0,IDUM,RDUM,IOUT,80)
         WRITE(81,'(A,A)') LINE(1:ISTOP),' ucode.pvl'
C
      ELSE
         N=LEN_TRIM(LINE)
         IF(N.GT.0) THEN
           WRITE(81,'(A)') LINE(1:N)
         ELSE
           WRITE(81,'(A)')
         END IF
      END IF
      GO TO 10
C
100   CLOSE(UNIT=80)
      CLOSE(UNIT=81)
      RETURN
      END
      SUBROUTINE UCODENAMEFILE()
C     ******************************************************************
C     WRITE MODFLOW NAME FILE FOR UCODE TO USE
C     Change name of PVAL file so that UCODE will not destroy the
C           parametervalues a user has specified for MODFLOW.
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      CHARACTER*80 LINE
C
      OPEN(UNIT=80,FILE=DSNAME(1:NDSNAM)//'.nam')
      OPEN(UNIT=81,FILE=DSNAME(1:NDSNAM)//'.nam.ucode')
      WRITE(81,'(A)') '# MODFLOW Name File for use by UCODE'
C
10    READ(80,'(A)',END=100) LINE
C  Skip the "# MODFLOW-2005 Name File" comment
      IF(LINE(1:13).EQ.'# MODFLOW-200') GO TO 10
      LLOC=1
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,1,IDUM,RDUM,IOUT,80)
      IF(LINE(ISTART:ISTOP).EQ.'PVAL') THEN
         CALL URWORD(LINE,LLOC,ISTART,ISTOP,0,IDUM,RDUM,IOUT,80)
         WRITE(81,'(A,A)') LINE(1:ISTOP),' ucode.pvl'
      ELSE
         N=LEN_TRIM(LINE)
         IF(N.GT.0) THEN
           WRITE(81,'(A)') LINE(1:N)
         ELSE
           WRITE(81,'(A)')
         END IF
      END IF
      GO TO 10
C
100   CLOSE(UNIT=80)
      CLOSE(UNIT=81)
      RETURN
      END
      SUBROUTINE UCODECONTROLFILE()
C     ******************************************************************
C     WRITE UCODE MAIN INPUT FILE
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      INCLUDE 'ucode.inc'
      CHARACTER*80 LINE
C
      CHARACTER*5 STATNAM(5)
      DATA STATNAM/'  VAR','   SD','   CV','   WT','SQRWT'/
      CHARACTER*2 LENNAM(0:3)
      DATA LENNAM/'NA','ft','m','cm'/
      CHARACTER*2 MASNAM(5)
      DATA MASNAM/'NA','mg','g','kg','lb'/
      CHARACTER*3 TIMNAM(0:5)
      DATA TIMNAM/'NA','s','min','hr','d','yr'/
      CHARACTER*3 NYANS(0:1)
      DATA NYANS/'no','yes'/
      CHARACTER*12 CSS(7)
      DATA CSS/'css','dss','onepercentss','allss','unscaled',
     1           'all','none'/
      CHARACTER*10 CREALM(2)
      DATA CREALM/'Native','Regression'/
      DIMENSION IPSGRP(MXPRIOR)
C     ------------------------------------------------------------------
      CALL PESTRANSLATE
C
      OPEN(UNIT=81,FILE=DSNAME(1:NDSNAM)//'_ucode.in')
      WRITE(81,'(A)')
     1   '# UCODE Main Input File generated by mfPEStoUCODE'
      WRITE(81,'(A)') '#'
C
      WRITE(81,'(A)') '# ------------------------------'
      WRITE(81,'(A)') '# UCODE-OPTION INFORMATION'
      WRITE(81,'(A)') '# ------------------------------'
      WRITE(81,'(A)') 'BEGIN OPTIONS KEYWORDS'
      WRITE(81,'(A,I1)') 'Verbose= ',VERBOSE
      WRITE(81,'(A)') 'END OPTIONS'
      WRITE(81,'(A)') '#'
C
      WRITE(81,'(A)') '# ------------------------------'
      WRITE(81,'(A)') '# UCODE-CONTROL INFORMATION'
      WRITE(81,'(A)') '# ------------------------------'
      WRITE(81,'(A)') 'BEGIN UCODE_CONTROL_DATA KEYWORDS'
      WRITE(81,'(A)') '  ModelName=       '//DSNAME(1:NDSNAM)
      CALL IVLGET('LENUNI',0,0,ITYPE,LENUNI)
      IF(ITYPE.NE.0) LENUNI=0
      WRITE(81,'(A,A)')  '  ModelLengthUnits= ',LENNAM(LENUNI)
      WRITE(81,'(A,A)')  '  ModelMassUnits=   ',MASNAM(MODELMASSUNITS)
      CALL IVLGET('ITMUNI',0,0,ITYPE,ITMUNI)
      IF(ITYPE.NE.0) ITMUNI=0
      WRITE(81,'(A,A)')  '  ModelTimeUnits=   ',TIMNAM(ITMUNI)
      WRITE(81,'(A)') '#Performance'
      WRITE(81,'(A)') '  sensitivities='//NYANS(SENSITIVITIES)//
     1                '       # calculate sensitiv1ities: yes/no'
      WRITE(81,'(A)') '  optimize='//NYANS(OPTIMIZE)//
     1             '               # estimate parameters: yes/no'
      WRITE(81,'(A)') '#Printing and output files'
      WRITE(81,'(A)') '  StartRes='//NYANS(STARTRES)//
     1              '                # print residuals: yes/no '
      WRITE(81,'(A)') '  IntermedRes='//NYANS(INTERMEDRES)//
     1              '             # # same'
      WRITE(81,'(A)') '  FinalRes='//NYANS(FINALRES)//
     1            '               # # same'
      WRITE(81,'(A)') '  StartSens='//CSS(STARTSENS)//
     1                   '     # print sensitivities:  '
      WRITE(81,'(A)') '  IntermedSens='//CSS(INTERMEDSENS)//
     1                   '  # #   css, dss, unscaled, onepercentss,'
      WRITE(81,'(A)') '  FinalSens='//CSS(FINALSENS)//
     1                   '     # #   allss,all, or none'
      WRITE(81,'(A)') '  DataExchange=yes'//
     1             '           # create data-exchange files: yes/no'
      WRITE(81,'(A)') 'END UCODE_CONTROL_DATA'
      WRITE(81,'(A)') '#'
C
      WRITE(81,'(A)') '# ------------------------------'
      WRITE(81,'(A)') '# REGRESSION-CONTROL INFORMATION'
      WRITE(81,'(A)') '# ------------------------------'
      WRITE(81,'(A)') 'BEGIN REG_GN_CONTROLS KEYWORDS'
      WRITE(81,18) '  tolpar=',TOLPAR,
     1   '        # GN param conv crit. Also see parameter blocks'
18    FORMAT(A,1P,E13.5,A)
      WRITE(81,18) '  tolsosc=',TOLSOSC,
     1    '       # GN fit-change conv c1riteria.'
      WRITE(81,19) '  maxiter=',MAXITER,
     1 '               # maximum # of GaussNewton updates'
19    FORMAT(A,I5,A)
      WRITE(81,18) '  maxchange=',MAXCHANGE,
     1      '     # max frac param change for GN updates'
      WRITE(81,'(A)') '  maxchangerealm='//CREALM(MAXCHANGEREALM)//
     1         '  # how changes apply, log-trans params'
      WRITE(81,18) '  MqrtDirection=',MQRTDIRECTION,
     1         '  # angle (degrees) for Mrqt parameter'
      WRITE(81,18) '  MqrtFactor=',MQRTFACTOR
      WRITE(81,18) '  MqrtIncrement=',MQRTINCREMENT
      WRITE(81,'(A)') '  QuasiNewton='//NYANS(QUASINEWTON)//
     1        '             # option to use QN updating: yes, no'
      WRITE(81,19) '  QNiter=',QNITER
      WRITE(81,18) '  QNsosr=',QNSOSR
      WRITE(81,'(A)')
     1    '  OmitDefault=   1            # Values read from .omit file'
      WRITE(81,'(A)') 'END REG_GN_CONTROLS'
      WRITE(81,'(A)') '#'
C
      WRITE(81,'(A)') '# --------------------------------'
      WRITE(81,'(A)') '# COMMAND FOR APPLICATION MODEL(S)'
      WRITE(81,'(A)') '# --------------------------------'
      WRITE(81,'(A)') 'BEGIN MODEL_COMMAND_LINES KEYWORDS'
      WRITE(81,'(A)')
     1   "  Command='mf2005 "//DSNAME(1:NDSNAM)//".nam.ucode'"
      WRITE(81,'(A)') '  Purpose=forward'
      WRITE(81,'(A)') "  CommandId='MODFLOW 2005'"
      WRITE(81,'(A)') 'END MODEL_COMMAND_LINES'
      WRITE(81,'(A)') '#'
C
      WRITE(81,'(A)') '# ---------------------'
      WRITE(81,'(A)') '# PARAMETER INFORMATION'
      WRITE(81,'(A)') '# ---------------------'
      WRITE(81,'(A)') 'BEGIN PARAMETER_DATA FILES'
      WRITE(81,'(A)') DSNAME(1:NDSNAM)//'_ucode.par'
      WRITE(81,'(A)') 'END PARAMETER_DATA'
      WRITE(81,'(A)') '#'
C
      WRITE(81,'(A)') '# -----------------------'
      WRITE(81,'(A)') '# OBSERVATION INFORMATION'
      WRITE(81,'(A)') '# -----------------------'
C  Make groups from the PlotSymbol values
      IF(IUNIT(28).GT.0 .OR. IUNIT(33).GT.0 .OR. IUNIT(34).GT.0 .OR.
     1   IUNIT(35).GT.0 .OR. IUNIT(38).GT.0) CALL OBSGROUP(81)
      WRITE(81,*)
      WRITE(81,'(A)') 'BEGIN OBSERVATION_DATA FILES'
      WRITE(81,'(A)') DSNAME(1:NDSNAM)//'_ucode.obs'
      WRITE(81,'(A)') 'END OBSERVATION_DATA'
      WRITE(81,'(A)') '#'
C
C  PRIOR INFORMATION
      NPRIOR=0
      NGRP=0
      DO 110 I=1,MXPRIOR
      IF(PriorName(I).NE.' ') THEN
        NPRIOR=NPRIOR+1
        IF(NGRP.GT.0) THEN
          DO 100 J=1,NGRP
          IF(IPSGRP(J).EQ.PVPlotSymbol(I)) GO TO 110
100       CONTINUE
        END IF
        NGRP=NGRP+1
        IPSGRP(NGRP)=PVPlotSymbol(I)
      END IF
110   CONTINUE
C
      IF(NPRIOR.GT.0) THEN
C
C  Write the prior information groups into the control file.
        WRITE(81,*) 
        WRITE(81,'(A)') '# ------------------------------------'
        WRITE(81,'(A)') '# LINEAR PRIOR INFORMATION INFORMATION'
        WRITE(81,'(A)') '# ------------------------------------'
        WRITE(81,'(A)') 'BEGIN PRIOR_INFORMATION_GROUPS TABLE'
        WRITE(81,'(A,I5,A)') 'nrow=',NGRP,'  ncol=2  columnlabels'
        WRITE(81,'(A)') 'GroupName       PlotSymbol'
        DO 200 I=1,NGRP
          WRITE(81,'(I10,1X,I10)') IPSGRP(I),IPSGRP(I)
200     CONTINUE
        WRITE(81,'(A)') 'END PRIOR_INFORMATION_GROUPS'
C
C  Write Prior information
        WRITE(81,*)
	WRITE(81,'(A)') 'BEGIN Linear_Prior_Information TABLE'
        WRITE(81,'(A,I5,A)') 'nrow=',NPRIOR,'  ncol=6  columnlabels'
        WRITE(81,'(A)') 'PriorName  PriorInfoValue   GroupName    '//
     1                  'Statistic      StatFlag   Equation'
        DO 300 I=1,MXPRIOR
        IF(PriorName(I).NE.' ') WRITE(81,281) PriorName(I),
     1         PriorInfoValue(I),PVPlotSymbol(I),PVStatistic(I),
     2         STATNAM(PVStatFlag(I)),TRIM(Equation(I))
281     FORMAT(A,1X,1PE13.5,I12,4X,E13.5,A10,5X,A)
300     CONTINUE
        WRITE(81,'(A)') 'END Linear_Prior_Information'
        WRITE(81,'(A)') '#'
      END IF
C
C  Application model data.
      WRITE(81,'(A)') '# -----------------------------'
      WRITE(81,'(A)') '# APPLICATION MODEL INFORMATION'
      WRITE(81,'(A)') '# -----------------------------'
      WRITE(81,'(A)') 'BEGIN MODEL_INPUT_FILES	KEYWORDS'
      WRITE(81,'(A)') '  modinfile=ucode.pvl'
      WRITE(81,'(A)') '  templatefile='//DSNAME(1:NDSNAM)//'_ucode.tpl'
      WRITE(81,'(A)') 'END MODEL_INPUT_FILES'
      WRITE(81,'(A)')
      WRITE(81,'(A)') 'BEGIN MODEL_OUTPUT_FILES  KEYWORDS'
      IF(IUNIT(27).GT.0) THEN
        N=LEN_TRIM(FILNAM(IUNIT(27)))
        WRITE(81,'(A)') '  modoutfile='//DSNAME(1:NDSNAM)//'._os'
      END IF
      WRITE(81,'(A)')
     1       '  instructionfile='//DSNAME(1:NDSNAM)//'_ucode.inst'
      WRITE(81,'(A)') '  category=obs'
      WRITE(81,'(A)') 'END MODEL_OUTPUT_FILES'
C
C
      CLOSE(UNIT=81)
      RETURN
      END
      SUBROUTINE UCODEPARFILE()
C     ******************************************************************
C     Write UCODE Parameter file and Template file
C     ******************************************************************
C
      INCLUDE 'mfi2k.inc'
      INCLUDE 'mfi2kpar.inc'
      CHARACTER*3 NOYES(0:1)
      DATA NOYES/'No ','Yes'/
C     ------------------------------------------------------------------
C  Parameter File
      OPEN(FILE=DSNAME(1:NDSNAM)//'_ucode.par',UNIT=81)
C
      NPINFILE=0
      DO 100 I=1,IPSUM
      IF(ISFILE(I).NE.0) THEN
         NPINFILE=NPINFILE+1
      END IF
100   CONTINUE

C
C  Start of file
      WRITE(81,'(A)') '#UCODE Parameter File'
      WRITE(81,'(A)') 'BEGIN PARAMETER_DATA TABLE'
      WRITE(81,'(A,I5,A)') 'nrow=',NPINFILE,'  ncol=8  columnlabels'
      WRITE(81,'(A)') 'Paramname    Startvalue   Lowervalue   '//
     1                'Uppervalue   Scalepval    '//
     2                'Transform  Adjustable '//
     3                'Senmethod'
C
C  Write data
      NPINFILE=0
      DO 200 I=1,IPSUM
      IF(ISFILE(I).NE.0) THEN
         NPINFILE=NPINFILE+1
         WRITE(81,107) PARNAM(I),BSTART(I),BL(I),BU(I),BSCAL(I),
     1                 NOYES(LN(I)),NOYES(ISENS(I)),1
107      FORMAT(A,1X,1P,4E13.5,5X,A3,8X,A3,I10)
      END IF
200   CONTINUE
C
C
      WRITE(81,'(A)') 'END PARAMETER_DATA'
      CLOSE(UNIT=81)
C
C  TEMPLATE FILE
      OPEN(FILE=DSNAME(1:NDSNAM)//'_ucode.tpl',UNIT=81)
C
C  Opening data
      WRITE(81,'(A)') 'jtf @'
      WRITE(81,'(A)') '#UCODE Template File'
      WRITE(81,'(I5)') NPINFILE
C
C  Write data
      DO 300 I=1,IPSUM
      IF(ISFILE(I).NE.0) THEN
         WRITE(81,207) PARNAM(I),PARNAM(I)
207      FORMAT(A,' @',A,'    @')
      END IF
300   CONTINUE
C
      CLOSE(UNIT=81)
C
      RETURN
      END
      SUBROUTINE UCODEOBSFILE()
C     ******************************************************************
C     Write UCODE Observation files and Instruction file
C     ******************************************************************
C
      INCLUDE 'mfi2k.inc'
      COMMON /FOBCOM/LAYFOB(MXCFOB),IROWFOB(MXCFOB),ICOLFOB(MXCFOB),
     1               FOBFACT(MXCFOB),FOBSTAT(MXTFOB),FOBS(MXTFOB),
     2               IFOBST(MXTFOB),IFOBRSP(MXTFOB),FOBTOFF(MXTFOB),
     3               IFOBPLT(MXTFOB),NGFOB,NTFOB,NCFOB,NFOB(6,MXGFOB),
     4               FOBCON(2,40)
      COMMON /CFOBCOM/FOBNAM
      CHARACTER*12 FOBNAM(MXTFOB)
C
      COMMON /SHOBCOM/LAYSHOB(10,MXHOB),IROWSHOB(MXHOB),ICOLSHOB(MXHOB),
     1               SHROFF(MXHOB),SHCOFF(MXHOB),SHOBSTAT(MXHOB),
     2               SHOBS(MXHOB),ISHOBST(MXHOB),ISHREFSP(MXHOB),
     3               SHOBTOFF(MXHOB),ISHOBPLT(MXHOB),SHOBPR(10,MXHOB)
      COMMON /CSHOBCOM/SHOBNAM
      CHARACTER*12 SHOBNAM(MXHOB)
      COMMON /HOBCOM/LAYHOB(10,MXHOB),IROWHOB(MXHOB),ICOLHOB(MXHOB),
     1               ROFF(MXHOB),COFF(MXHOB),HOBSTAT(MXHOB),HOBS(MXHOB),
     2               IHOBST(MXHOB),IREFSP(MXHOB),HOBTOFF(MXHOB),
     3               IHOBPLT(MXHOB),ITT(MXHOB),DDSTAT(MXHOB),
     4               HOBPR(10,MXHOB)
      COMMON /CHOBCOM/HOBNAM
      CHARACTER*12 HOBNAM(MXHOB),HNAM
C
      CHARACTER*5 STATNAM(5)
      DATA STATNAM/'  VAR','   SD','   CV','   WT','SQRWT'/
C     ------------------------------------------------------------------
C
C  Count head observations
      NH=0
C
C  Count observations
      NH=0
      MOBS=0
      MAXM=0
C
      DO 10 I=1,MXHOB
C  Single time
      IF(SHOBNAM(I).NE.' ') THEN
         NH=NH+1
      END IF
C  Multi-Time
      IF(HOBNAM(I).NE.' ') THEN
         IF(ITT(I).EQ.3) NH=NH+1
      END IF
10    CONTINUE
C
C  Count flow observations
      NOBSDRN=0
      NOBSRIV=0
      NOBSGHB=0
      NOBSCHD=0
      DO 50 I=1,NGFOB
      IF(NFOB(1,I).EQ.33) NOBSDRN=NOBSDRN+NFOB(3,I)-NFOB(2,I)+1
      IF(NFOB(1,I).EQ.34) NOBSRIV=NOBSRIV+NFOB(3,I)-NFOB(2,I)+1
      IF(NFOB(1,I).EQ.35) NOBSGHB=NOBSGHB+NFOB(3,I)-NFOB(2,I)+1
      IF(NFOB(1,I).EQ.38) NOBSCHD=NOBSCHD+NFOB(3,I)-NFOB(2,I)+1
50    CONTINUE
      NOBSTOT=NH+NOBSDRN+NOBSRIV+NOBSGHB+NOBSCHD
C
C  Open UCODE Observation and Instruction Files
      OPEN(FILE=DSNAME(1:NDSNAM)//'_ucode.obs',UNIT=81)
      OPEN(FILE=DSNAME(1:NDSNAM)//'_ucode.inst',UNIT=82)
C
C  Initial lines of Ucode Observation file
      WRITE(81,'(A)') '#UCODE Observation File'
      WRITE(81,'(A)') 'BEGIN OBSERVATION_DATA TABLE'
      WRITE(81,'(A,I5,A)') 'nrow=',NOBSTOT,'  ncol=5  columnlabels'
      WRITE(81,'(A)') 'ObsName       ObsValue     Statistic  '//
     1                 'StatFlag  GroupName'
C
C  Initial lines of Instruction file
      WRITE(82,'(A)') 'jif @'
      WRITE(82,'(A,I5)') 'StandardFile 1  1 ',NOBSTOT
C
C
C  Write data
C  Single-Time Observations
      DO 110 I=1,MXHOB
      IF(SHOBNAM(I).NE.' ')  THEN
         WRITE(81,149) SHOBNAM(I),SHOBS(I),
     3            SHOBSTAT(I),STATNAM(ISHOBST(I)),ISHOBPLT(I)
         WRITE(82,149) SHOBNAM(I)
      END IF
110   CONTINUE
C
C  Multi-Time Observations
      DO 130 I=1,MXHOB
      IF(HOBNAM(I).NE.' ') THEN
        IF(ITT(I).LT.3) THEN
C  See if multi-time observations are drawdowns or heads
          HBASE=HOBS(I+1)
          IBASE=I+1
          IDDN=ITT(I)
        ELSE
          IF(IDDN.EQ.2 .AND. I.NE.IBASE) THEN
              WRITE(81,149) HOBNAM(I),HOBS(I)-HBASE,
     2            HOBSTAT(I),STATNAM(IHOBST(I)),IHOBPLT(I)
          ELSE
              WRITE(81,149) HOBNAM(I),HOBS(I),
     2            HOBSTAT(I),STATNAM(IHOBST(I)),IHOBPLT(I)
          END IF
          WRITE(82,149) HOBNAM(I)
        END IF
      END IF
130   CONTINUE
C
C  Flow observations
      DO 150 I=1,NGFOB
      IF(NFOB(1,I).EQ.33) THEN
         DO 148 J=NFOB(2,I),NFOB(3,I)
         WRITE(81,149) FOBNAM(J),FOBS(J),FOBSTAT(J),
     1                 STATNAM(IFOBST(J)),IFOBPLT(J)
         WRITE(82,149) FOBNAM(J)
148      CONTINUE
149      FORMAT(A,1X,1P,2E13.5,1X,A,I10)
      END IF
150   CONTINUE
C
      DO 160 I=1,NGFOB
      IF(NFOB(1,I).EQ.34) THEN
         DO 159 J=NFOB(2,I),NFOB(3,I)
         WRITE(81,149) FOBNAM(J),FOBS(J),FOBSTAT(J),
     1                 STATNAM(IFOBST(J)),IFOBPLT(J)
         WRITE(82,149) FOBNAM(J)
159      CONTINUE
      END IF
160   CONTINUE
C
      DO 170 I=1,NGFOB
      IF(NFOB(1,I).EQ.35) THEN
         DO 169 J=NFOB(2,I),NFOB(3,I)
         WRITE(81,149) FOBNAM(J),FOBS(J),FOBSTAT(J),
     1                 STATNAM(IFOBST(J)),IFOBPLT(J)
         WRITE(82,149) FOBNAM(J)
169      CONTINUE
      END IF
170   CONTINUE
C
      DO 180 I=1,NGFOB
      IF(NFOB(1,I).EQ.38) THEN
         DO 179 J=NFOB(2,I),NFOB(3,I)
         WRITE(81,149) FOBNAM(J),FOBS(J),FOBSTAT(J),
     1                 STATNAM(IFOBST(J)),IFOBPLT(J)
         WRITE(82,149) FOBNAM(J)
179      CONTINUE
      END IF
180   CONTINUE
C
      WRITE(81,'(A)') 'END OBSERVATION_DATA'
      CLOSE(UNIT=81)
      CLOSE(UNIT=82)
C
      OPEN(FILE=DSNAME(1:NDSNAM)//'_ucode.omit',UNIT=81)
      CALL VALGET('HOBDRY',0,0,ITYPE,HOBDRY)
      IF(ITYPE.NE.0) HOBDRY=1.E30
      WRITE(81,'(A)')
     1  '# OmitDefault values -- HOBDRY from MODFLOW HOB file'
      WRITE(81,'(A)') 'BEGIN OMIT_DATA'
      WRITE(81,'(1PE13.5)') HOBDRY
      WRITE(81,'(A)') 'END OMIT_DATA'
      CLOSE(UNIT=81)
C
      RETURN
      END
      SUBROUTINE OBSGROUP(IU)
C  Create UCODE observation groups using the Plot Symbol values assigned to observations
      PARAMETER (MXGRP=500)
      DIMENSION IPSGRP(MXGRP)
      INCLUDE 'mfi2k.inc'
      COMMON /SHOBCOM/LAYSHOB(10,MXHOB),IROWSHOB(MXHOB),ICOLSHOB(MXHOB),
     1               SHROFF(MXHOB),SHCOFF(MXHOB),SHOBSTAT(MXHOB),
     2               SHOBS(MXHOB),ISHOBST(MXHOB),ISHREFSP(MXHOB),
     3               SHOBTOFF(MXHOB),ISHOBPLT(MXHOB),SHOBPR(10,MXHOB)
      COMMON /CSHOBCOM/SHOBNAM
      CHARACTER*12 SHOBNAM(MXHOB)
      COMMON /HOBCOM/LAYHOB(10,MXHOB),IROWHOB(MXHOB),ICOLHOB(MXHOB),
     1               ROFF(MXHOB),COFF(MXHOB),HOBSTAT(MXHOB),HOBS(MXHOB),
     2               IHOBST(MXHOB),IREFSP(MXHOB),HOBTOFF(MXHOB),
     3               IHOBPLT(MXHOB),ITT(MXHOB),DDSTAT(MXHOB),
     4               HOBPR(10,MXHOB)
      COMMON /CHOBCOM/HOBNAM
      CHARACTER*12 HOBNAM(MXHOB),HNAM
      COMMON /FOBCOM/LAYFOB(MXCFOB),IROWFOB(MXCFOB),ICOLFOB(MXCFOB),
     1               FOBFACT(MXCFOB),FOBSTAT(MXTFOB),FOBS(MXTFOB),
     2               IFOBST(MXTFOB),IFOBRSP(MXTFOB),FOBTOFF(MXTFOB),
     3               IFOBPLT(MXTFOB),NGFOB,NTFOB,NCFOB,NFOB(6,MXGFOB),
     4               FOBCON(2,40)
      COMMON /CFOBCOM/FOBNAM
      CHARACTER*12 FOBNAM(MXTFOB)      
C
      NGRP=0
C  Single-Time Observations
      DO 110 I=1,MXHOB
      IF(SHOBNAM(I).NE.' ')  THEN
        IF(NGRP.GT.0) THEN
          DO 100 J=1,NGRP
          IF(IPSGRP(J).EQ.ISHOBPLT(I)) GO TO 110
100       CONTINUE
        END IF
        NGRP=NGRP+1
        IF(NGRP.GT.MXGRP) THEN
           WRITE(*,*)
     1    'Maximum number of different plot symbols has been exceeded'
           STOP
        END IF
        IPSGRP(NGRP)=ISHOBPLT(I)
      END IF
110   CONTINUE
C
C  Multi-Time Observations
      DO 130 I=1,MXHOB
      IF(HOBNAM(I).NE.' ') THEN
        IF(ITT(I).EQ.3) THEN
          IF(NGRP.GT.0) THEN
            DO 125 J=1,NGRP
            IF(IPSGRP(J).EQ.IHOBPLT(I)) GO TO 130
125         CONTINUE
          END IF
          NGRP=NGRP+1
          IF(NGRP.GT.MXGRP) THEN
             WRITE(*,*)
     1    'Maximum number of different plot symbols has been exceeded'
             STOP
          END IF
          IPSGRP(NGRP)=IHOBPLT(I)
        END IF
      END IF
130   CONTINUE
C
C  Flow observations
      DO 150 I=1,NGFOB
      IF(NFOB(1,I).EQ.33 .OR. NFOB(1,I).EQ.34 .OR.
     1   NFOB(1,I).EQ.35 .OR. NFOB(1,I).EQ.38 ) THEN
        DO 148 K=NFOB(2,I),NFOB(3,I)
          IF(NGRP.GT.0) THEN
            DO 145 J=1,NGRP
            IF(IPSGRP(J).EQ.IFOBPLT(K)) GO TO 148
145         CONTINUE
          END IF
          NGRP=NGRP+1
          IF(NGRP.GT.MXGRP) THEN
             WRITE(*,*)
     1    'Maximum number of different plot symbols has been exceeded'
             STOP
          END IF
          IPSGRP(NGRP)=IFOBPLT(K)
148     CONTINUE
      END IF
150   CONTINUE
C
C  Write the observation groups into the control file.
      WRITE(IU,*) 
      WRITE(IU,'(A)') 'BEGIN OBSERVATION_GROUPS TABLE'
      WRITE(IU,'(A,I5,A)') 'nrow=',NGRP,'  ncol=2  columnlabels'
      WRITE(IU,'(A)') 'GroupName       PlotSymbol'
      DO 200 I=1,NGRP
        WRITE(IU,'(I10,1X,I10)') IPSGRP(I),IPSGRP(I)
200   CONTINUE
      WRITE(IU,'(A)') 'END OBSERVATION_GROUPS'
C
      RETURN
      END
      SUBROUTINE PESTRANSLATE()
C     ******************************************************************
C     TRANSLATE PES VARIABLE TO UCODE
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      INCLUDE 'ucode.inc'
      INCLUDE 'mfi2kpar.inc'
      COMMON /PESCOM/PRM(200),PRMSP(200),IPRMSF(200),IPRMPS(200)
      COMMON /CPESCOM/PARNEG(200),EQNAM(200),PESEQ(200)
      CHARACTER*10 PARNEG,EQNAM
      CHARACTER*100 PESEQ
C     ------------------------------------------------------------------
C  Set defaults for UCODE
      Verbose=3
      ModelMassUnits=1
      Sensitivities=0
      Optimize=0
      Linearity=0
      Prediction=0
      LinearityAdv=0
      EigenValues=1
      StartRes=1
      IntermedRes=0
      FinalRes=1
      StartSens=2
      IntermedSens=7
      FinalSens=2
      TolPar=.01
      TolSOSC=0.
      MaxIter=10
      MaxChange=2.0
      MaxChangerealm=1
      MqrtFactor=1.5
      MqrtDirection=85.4
      MqrtIncrement=.001
      QuasiNewton=0
      QNiter=5
      QNsosr=.01
      OmitInsensitive=0
      MinimumSensRatio=0.005
      ReincludeSensRatio=0.02
      TrustRegion=1
      MaxStep=-1.0
      ConsecMax=5
      SOSsurface=0
      SOSIncrement=5
      CreateInitFiles=0
C
      DO 10 I=1,MXPRIOR
      PRIORINFOVALUE(I)=0.
      PVStatistic(I)=1.0
      PVStatFlag(I)=1
      PVPlotSymbol(I)=0
      PriorName(I)=' '
      Equation(I)=' '
10    CONTINUE
C
      IF(IUNIT(25).GT.0) Sensitivities=1
      IF(IUNIT(26).GT.0) THEN
         Optimize=1
         Sensitivities=0
      END IF
C
      CALL IVLGET('ITMXP',0,0,ITYPE,ITMXP)
      IF(ITYPE.NE.0) ITMXP=5
      MaxIter=ITMXP
C
      CALL VALGET('DMAX',0,0,ITYPE,DMAX)
      IF(ITYPE.NE.0) DMAX=2.0
      MaxChange=DMAX
C
      CALL VALGET('TOL',0,0,ITYPE,TOL)
      IF(ITYPE.NE.0) TOL=.01
      TolPar=TOL
C
      CALL VALGET('SOSC',0,0,ITYPE,SOSC)
      IF(ITYPE.NE.0) SOSC=0.
      TolSOSC=SOSC
C
      CALL IVLGET('IBEFLG',0,0,ITYPE,IBEFLG)
      IF(ITYPE.NE.0) IBEFLG=0
      IF(IBEFLG.EQ.2) THEN
        Sensitivities=0
        Optimize=0
        Linearity=1
      END IF
C
      CALL IVLGET('IYCFLG',0,0,ITYPE,IYCFLG)
      IF(ITYPE.NE.0) IYCFLG=0
      IF(IYCFLG.EQ.1) THEN
        Sensitivities=0
        Optimize=0
        Linearity=1
      END IF
C
      CALL IVLGET('IOSTAR',0,0,ITYPE,IOSTAR)
      IF(ITYPE.NE.0) IOSTAR=0
      IF(IOSTAR.EQ.1) Verbose=0
C
      CALL IVLGET('NOPT',0,0,ITYPE,NOPT)
      IF(ITYPE.NE.0)  NOPT=0
      QuasiNewton=NOPT
C
      CALL IVLGET('NFIT',0,0,ITYPE,NFIT)
      IF(ITYPE.NE.0) NFIT=5
      QNiter=NFIT
C
      CALL VALGET('SOSR',0,0,ITYPE,SOSR)
      IF(ITYPE.NE.0) SOSR=.01
      QNsosr=SOSR
C
      CALL IVLGET('IAP',0,0,ITYPE,IAP)
      IF(ITYPE.NE.0) IAP=0
      MaxChangeRealm=IAP+1
C
      CALL IVLGET('IPRINT',0,0,ITYPE,IPRINT)
      IF(ITYPE.NE.0) IPRINT=0
      StartRes=1
      IntermedRes=IPRINT
      FinalRes=1
      StartSens=2
      IntermedSens=7
      FinalSens=2
C
      CALL IVLGET('LPRINT',0,0,ITYPE,LPRINT)
      IF(ITYPE.NE.0) LPRINT=0
      EigenValues=LPRINT
C
      CALL VALGET('RMAR',0,0,ITYPE,RMAR)
      IF(ITYPE.NE.0) RMAR=.001
      MqrtIncrement=RMAR
C
      CALL VALGET('RMARM',0,0,ITYPE,RMARM)
      IF(ITYPE.NE.0) RMARM=1.5
      MqrtFactor=RMARM
C
      CALL VALGET('CSA',0,0,ITYPE,CSA)
      IF(ITYPE.NE.0) CSA=.08
      MqrtDirection=90.0-CSA
C
      DO 20 I=1,200
      IF(EQNAM(I).NE.' ') THEN
        PriorName(I)=EQNAM(I)
        PriorInfoValue(I)=PRM(I)
        Equation(I)=PESEQ(I)
        PVStatistic(I)=PRMSP(I)
        PVStatFlag(I)=IPRMSF(I)
        PVPlotSymbol(I)=IPRMPS(I)
      END IF
20    CONTINUE
C
      RETURN
      END
      SUBROUTINE GLOBALDF(INDIS)
C
C     ******************************************************************
C     GLOBAL DEFINITION
C     ******************************************************************
C
C        SPECIFICATIONS:
C     ------------------------------------------------------------------
      INCLUDE 'mfi2k.inc'
      INCLUDE 'mfi2kpar.inc'
      CHARACTER*80 LINE
C     ------------------------------------------------------------------
C  Initialize parameter definition variables.
      IPSUM=0
      ICLSUM=0
      IDEFPAR=0
      DO 5 N=1,MXPAR
        PARNAM(N)=' '
        PARTYP(N)=' '
        IPLOC(1,N)=0
        IPLOC(2,N)=0
        IACTIVE(N)=0
    5 CONTINUE
C
C------Check for existence of discretization file
      IF(INDIS.LE.0) THEN
         WRITE(IOUT,*) ' DIS file must be specified for MODFLOW to run'
         CALL RDABRT
      END IF
      WRITE(IOUT,6) INDIS
    6 FORMAT(1X,/1X,'DISCRETIZATION INPUT DATA READ FROM UNIT',I3)
      OPEN(UNIT=IUMISC,FILE=FILNAM(INDIS))
C
C5------READ NUMBER OF LAYERS, ROWS, COLUMNS, STRESS PERIODS, AND
C5------ITMUNI USING FREE OR FIXED FORMAT.
C
C2------READ FIRST RECORD AND WRITE
      CALL URDCOM(IUMISC,LINE,24)
      LLOC=1
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,NLAY,R,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,NROW,R,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,NCOL,R,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,NPER,R,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,ITMUNI,R,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,LENUNI,R,IOUT,IN)
C
C6------PRINT # OF LAYERS, ROWS, COLUMNS AND STRESS PERIODS.
      WRITE(IOUT,7) NLAY,NROW,NCOL
    7 FORMAT(1X,I4,' LAYERS',I10,' ROWS',I10,' COLUMNS')
      WRITE(IOUT,8) NPER
    8 FORMAT(1X,I3,' STRESS PERIOD(S) IN SIMULATION')
C
C7------SELECT AND PRINT A MESSAGE SHOWING TIME UNIT.
      IF(ITMUNI.LT.0 .OR. ITMUNI.GT.5) ITMUNI=0
      IF(ITMUNI.EQ.0) THEN
         WRITE(IOUT,10)
   10    FORMAT(1X,'MODEL TIME UNIT IS UNDEFINED')
      ELSE IF(ITMUNI.EQ.1) THEN
         WRITE(IOUT,11)
   11    FORMAT(1X,'MODEL TIME UNIT IS SECONDS')
      ELSE IF(ITMUNI.EQ.2) THEN
         WRITE(IOUT,21)
   21    FORMAT(1X,'MODEL TIME UNIT IS MINUTES')
      ELSE IF(ITMUNI.EQ.3) THEN
         WRITE(IOUT,31)
   31    FORMAT(1X,'MODEL TIME UNIT IS HOURS')
      ELSE IF(ITMUNI.EQ.4) THEN
         WRITE(IOUT,41)
   41    FORMAT(1X,'MODEL TIME UNIT IS DAYS')
      ELSE
         WRITE(IOUT,51)
   51    FORMAT(1X,'MODEL TIME UNIT IS YEARS')
      END IF
C
C7------SELECT AND PRINT A MESSAGE SHOWING LENGTH UNIT.
      IF(LENUNI.LT.0 .OR. LENUNI.GT.3) LENUNI=0
      IF(LENUNI.EQ.0) THEN
         WRITE(IOUT,61)
   61    FORMAT(1X,'MODEL LENGTH UNIT IS UNDEFINED')
      ELSE IF(LENUNI.EQ.1) THEN
         WRITE(IOUT,71)
   71    FORMAT(1X,'MODEL LENGTH UNIT IS FEET')
      ELSE IF(LENUNI.EQ.2) THEN
         WRITE(IOUT,81)
   81    FORMAT(1X,'MODEL LENGTH UNIT IS METERS')
      ELSE IF(LENUNI.EQ.3) THEN
         WRITE(IOUT,91)
   91    FORMAT(1X,'MODEL LENGTH UNIT IS CENTIMETERS')
      END IF
      CALL IVLSAV('ITMUNI',0,0,ITMUNI)
      CALL IVLSAV('LENUNI',0,0,LENUNI)
      CLOSE(UNIT=IUMISC)
C
      RETURN
      END
      SUBROUTINE UCODESV
C     ******************************************************************
C     Write data for UCODE main input file into a separate file used by MFI
C     ******************************************************************
      INCLUDE 'ucode.inc'
      INCLUDE 'mfi2k.inc'
      INCLUDE 'mfi2kpar.inc'
      COMMON /SHOBCOM/LAYSHOB(10,MXHOB),IROWSHOB(MXHOB),ICOLSHOB(MXHOB),
     1               SHROFF(MXHOB),SHCOFF(MXHOB),SHOBSTAT(MXHOB),
     2               SHOBS(MXHOB),ISHOBST(MXHOB),ISHREFSP(MXHOB),
     3               SHOBTOFF(MXHOB),ISHOBPLT(MXHOB),SHOBPR(10,MXHOB)
      COMMON /CSHOBCOM/SHOBNAM
      CHARACTER*12 SHOBNAM(MXHOB)
      COMMON /HOBCOM/LAYHOB(10,MXHOB),IROWHOB(MXHOB),ICOLHOB(MXHOB),
     1               ROFF(MXHOB),COFF(MXHOB),HOBSTAT(MXHOB),HOBS(MXHOB),
     2               IHOBST(MXHOB),IREFSP(MXHOB),HOBTOFF(MXHOB),
     3               IHOBPLT(MXHOB),ITT(MXHOB),DDSTAT(MXHOB),
     4               HOBPR(10,MXHOB)
      COMMON /CHOBCOM/HOBNAM
      CHARACTER*12 HOBNAM(MXHOB),HNAM
      COMMON /FOBCOM/LAYFOB(MXCFOB),IROWFOB(MXCFOB),ICOLFOB(MXCFOB),
     1               FOBFACT(MXCFOB),FOBSTAT(MXTFOB),FOBS(MXTFOB),
     2               IFOBST(MXTFOB),IFOBRSP(MXTFOB),FOBTOFF(MXTFOB),
     3               IFOBPLT(MXTFOB),NGFOB,NTFOB,NCFOB,NFOB(6,MXGFOB),
     4               FOBCON(2,40)
      COMMON /CFOBCOM/FOBNAM
      CHARACTER*12 FOBNAM(MXTFOB)      
C
      OPEN(UNIT=IUMISC,FILE=DSNAME(1:NDSNAM)//'.mfi')
      WRITE(IUMISC,'(A)') 'MFI file for saving UCODE control data'
      WRITE(IUMISC,*) 'Do not modify the file structure'
      WRITE(IUMISC,*) Verbose,'           Verbose'
      WRITE(IUMISC,*) ModelMassUnits,'           ModelMassUnits'
      WRITE(IUMISC,*) Sensitivities,'           Sensitivities'
      WRITE(IUMISC,*) Optimize,'           Optimize'
      WRITE(IUMISC,*) Linearity,'           Linearity'
      WRITE(IUMISC,*) Prediction,'           Prediction'
      WRITE(IUMISC,*) LinearityAdv,'           LinearityAdv'
      WRITE(IUMISC,*) EigenValues,'           EigenValues'
      WRITE(IUMISC,*) StartRes,'           StartRes'
      WRITE(IUMISC,*) IntermedRes,'           IntermedRes'
      WRITE(IUMISC,*) FinalRes,'           Finalres'
      WRITE(IUMISC,*) StartSens,'           StartSens'
      WRITE(IUMISC,*) IntermedSens,'           IntermedSens'
      WRITE(IUMISC,*) FinalSens,'           FinalSens'
      WRITE(IUMISC,2) TolPar,' TolPar'
   2  FORMAT(1X,1p,E12.5,A)
      WRITE(IUMISC,2) TolSOSC,' TolSOSC'
      WRITE(IUMISC,*) MaxIter,'           MaxIter'
      WRITE(IUMISC,2) MaxChange,' MaxChange'
      WRITE(IUMISC,*) MaxChangerealm,'           MaxChangerealm'
      WRITE(IUMISC,2) MqrtFactor,' MqrtFactor'
      WRITE(IUMISC,2) MqrtDirection,' MqrtDirection'
      WRITE(IUMISC,2) MqrtIncrement,' MqrtIncrement'
      WRITE(IUMISC,*) QuasiNewton,'           QuasiNewton'
      WRITE(IUMISC,*) QNiter,'           QNiter'
      WRITE(IUMISC,2) QNsosr,' QNsosr'
      NPRIOR=0
      DO 15 I=1,200
      IF(PriorName(I).NE.' ') NPRIOR=NPRIOR+1
15    CONTINUE
      WRITE(IUMISC,*) NPRIOR,'           NPRIOR'
      DO 20 I=1,200
      IF(PriorName(I).NE.' ') THEN
         WRITE(IUMISC,*) PriorInfoValue(I),' PriorInfoValue'
         WRITE(IUMISC,*) PVStatistic(I),' PVStatistic'
         WRITE(IUMISC,*) PVStatFlag(I),' PVStatFlag'
         WRITE(IUMISC,*) PVPlotSymbol(I),' PVPlotSymbol'
         WRITE(IUMISC,'(A)') PriorName(I)
         WRITE(IUMISC,'(A)') TRIM(Equation(I))
      END IF
20    CONTINUE
      WRITE(IUMISC,*) OmitInsensitive,'    OmitInsensitive'
      WRITE(IUMISC,2) MinimumSensRatio,'   MinimumSensRatio'
      WRITE(IUMISC,2) ReincludeSensRatio,'   ReincludeSensRatio'
      WRITE(IUMISC,*) TrustRegion,'      TrustRegion'
      WRITE(IUMISC,2) MaxStep,'          MaxStep'
      WRITE(IUMISC,*) ConsecMax,'      ConsecMax'
      WRITE(IUMISC,*) SOSsurface,'     SOSsurface'
      WRITE(IUMISC,*) SOSIncrement,'      SOSIncrement'
      WRITE(IUMISC,*) CreateInitFiles,'     CreateInitFiles'
C
C  Write Parameter data
      WRITE(IUMISC,*) IPSUM,'  UCODE Parameters'
      DO 33 I=1,IPSUM
      WRITE(IUMISC,32) PARNAM(I),BSTART(I),BL(I),BU(I),BSCAL(I),
     1        LN(I),ISENS(I),ISTRAIN(I),VALLOW(I),VALUP(I)
32    FORMAT(A,1X,1P,4E13.5,2I2,I5,2E13.5)
33    CONTINUE
C
C  Write Flow obs data
      NN=0
      DO 50 I=1,MXTFOB
      IF(FOBNAM(I).NE.' ')NN=NN+1
50    CONTINUE
      WRITE(IUMISC,*) NN,'  Flow observations'
      DO 60 I=1,MXTFOB
      IF(FOBNAM(I).NE.' ') WRITE(IUMISC,'(A,1P,E14.6,2I10)')
     1        FOBNAM(I),FOBSTAT(I),IFOBST(I),IFOBPLT(I)
60    CONTINUE
C
C  Write Head observation data
      NN=0
      DO 70 I=1,MXHOB
C  Single time
      IF(SHOBNAM(I).NE.' ') NN=NN+1
C  Multi-Time
      IF(HOBNAM(I).NE.' ') THEN
         IF(ITT(I).EQ.3) NN=NN+1
      END IF
70    CONTINUE
      WRITE(IUMISC,*) NN,'  Head observations'
      DO 80 I=1,MXHOB
C  Single time
      IF(SHOBNAM(I).NE.' ')  WRITE(IUMISC,'(A,1P,E14.6,2I10)')
     1        SHOBNAM(I),SHOBSTAT(I),ISHOBST(I),ISHOBPLT(I)
C  Multi-Time
      IF(HOBNAM(I).NE.' ') THEN
         IF(ITT(I).EQ.3) WRITE(IUMISC,'(A,1P,E14.6,2I10)')
     1        HOBNAM(I),HOBSTAT(I),IHOBST(I),IHOBPLT(I)
      END IF
80    CONTINUE
C
C
      CLOSE(UNIT=IUMISC)
C
      RETURN
      END
