      SUBROUTINE FOBIN(IULOC,CPACK)
C     ******************************************************************
C     Read Flow Observations -- MF2K version
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      COMMON /FOBCOM/LAYFOB(MXCFOB),IROWFOB(MXCFOB),ICOLFOB(MXCFOB),
     1               FOBFACT(MXCFOB),FOBSTAT(MXTFOB),FOBS(MXTFOB),
     2               IFOBST(MXTFOB),IFOBRSP(MXTFOB),FOBTOFF(MXTFOB),
     3               IFOBPLT(MXTFOB),NGFOB,NTFOB,NCFOB,NFOB(6,MXGFOB),
     4               FOBCON(2,40)
      COMMON /CFOBCOM/FOBNAM
      CHARACTER*12 FOBNAM(MXTFOB)      
      CHARACTER*(*) CPACK
      CHARACTER*200 LINE
C     ------------------------------------------------------------------
      IN=IUNIT(IULOC)
      OPEN(UNIT=IUMISC,FILE=FILNAM(IN))
C
C1------IDENTIFY PROCESS AND PRINT HEADING
      WRITE (IOUT,501) CPACK,IN
  501 FORMAT (/,A,' FLOW OBSERVATIONS, ',
     &        ' INPUT READ FROM UNIT ',I3)
C
C-------READ & PRINT ITEM 1 OF THE FOB INPUT FILE
      CALL URDCOM(IUMISC,LINE,IULOC)
      LLOC = 1
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,NQ,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,NQC,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,NQT,DUM,IOUT,IN)
      WRITE (IOUT,505) NQ
  505 FORMAT (/,' NUMBER OF CELL GROUPS:',I5)
      IF(NGFOB+NQ .GT. MXGFOB) THEN
         WRITE(IOUT,*)
     1  'EXCEEDS THE NUMBER OF CELL GROUPS SUPPORTED BY THIS PROGRAM:',
     2   MXGFOB
         CALL RDABRT
      END IF
C
C
C     READ ITEM 2:
      READ (IUMISC,*) FOBCON(1,IULOC),FOBCON(2,IULOC),IOWTQ
      IF(IOWTQ.GT.0) THEN
         WRITE(IOUT,*)
     1     'This program does not support the WTQ matrix'
         CALL RDABRT
      END IF
C
C  Read flow observations
      DO 100 I=1,NQ
      NGFOB=NGFOB+1
      NFOB(1,NGFOB)=IULOC
      READ(IUMISC,*) NQOB,NQCL
      NFOB(6,NGFOB)=0
      IF(NQCL.LT.0) THEN
         NQCL=-NQCL
         NFOB(6,NGFOB)=1
      END IF
      IF(NTFOB+NQOB.GT.MXTFOB) THEN
         WRITE(IOUT,*) MXTFOB,
     1' is the max. number of flow-obs. times supported by this program'
         CALL RDABRT
      END IF
      NFOB(2,NGFOB)=NTFOB+1
      WRITE (IOUT,525) I, CPACK
  525 FORMAT (/,'   GROUP NUMBER: ',I3,'   BOUNDARY TYPE: ',A,//,
     &40X,'OBSERVED',/,
     &20X,'REFER.',16X,'FLOW',/,
     &7X,'OBSERVATION',2X,'STRESS',4X,'TIME',5X,'GAIN (-) OR',14X,
     &'STATISTIC   PLOT',/,
     &2X,'OBS#    NAME',6X,'PERIOD   OFFSET',5X,'LOSS (+)',
     &4X,'STATISTIC     TYPE      SYM.')
      DO 60 J=1,NQOB
      NTFOB=NTFOB+1
      READ(IUMISC,*) FOBNAM(NTFOB),IFOBRSP(NTFOB),FOBTOFF(NTFOB),
     1             FOBS(NTFOB),FOBSTAT(NTFOB),IFOBST(NTFOB),
     2             IFOBPLT(NTFOB)
      CALL UTL_CHECK_NAMES(IOUT,FOBNAM(NTFOB))
      IF(IFOBST(NTFOB).LT.0 .OR. IFOBST(NTFOB).GT.2) IFOBST(NTFOB)=0
      WRITE (IOUT,535) J,FOBNAM(NTFOB),IFOBRSP(NTFOB),FOBTOFF(NTFOB),
     1             FOBS(NTFOB),FOBSTAT(NTFOB),IFOBST(NTFOB),
     2             IFOBPLT(NTFOB)
  535 FORMAT (1X,I5,1X,A12,2X,I4,2X,G11.4,1X,G11.4,1X,G11.4,2X,I10,
     &1X,I5)
C  ucode uses different stat-flag codes
      IFOBST(NTFOB)=IFOBST(NTFOB)+1
60    CONTINUE
      NFOB(3,NGFOB)=NTFOB
C
      IF(NCFOB+NQCL.GT.MXCFOB) THEN
         WRITE(IOUT,*) MXCFOB,
     1   ' is the max. flow-obs. locations supported by this program'
         CALL RDABRT
      END IF
      NFOB(4,NGFOB)=NCFOB+1
      WRITE(IOUT,540)
  540 FORMAT (/,'       LAYER  ROW  COLUMN    FACTOR')
      DO 80 J=1,NQCL
      NCFOB=NCFOB+1
      READ(IUMISC,*) LAYFOB(NCFOB),IROWFOB(NCFOB),ICOLFOB(NCFOB),
     1       FOBFACT(NCFOB)
      WRITE (IOUT,550)   LAYFOB(NCFOB),IROWFOB(NCFOB),ICOLFOB(NCFOB),
     1       FOBFACT(NCFOB)
  550 FORMAT (4X,I5,I7,I8,F10.2)
80    CONTINUE
      NFOB(5,NGFOB)=NCFOB
C
100   CONTINUE
C
      CLOSE(UNIT=IUMISC)
      RETURN
      END
      SUBROUTINE OBSIN
C     ******************************************************************
C     Read OBS file -- MF2K version
C     ******************************************************************
C
C        SPECIFICATIONS:
      INCLUDE 'mfi2k.inc'
      COMMON /OBSCOM/OUTNAM
      CHARACTER*78 OUTNAM
      CHARACTER*78 LINE
C     ------------------------------------------------------------------
      IULOC=27
      IN=IUNIT(IULOC)
      OPEN(UNIT=IUMISC,FILE=FILNAM(IN))
C
C1------IDENTIFY PROCESS AND PRINT HEADING
      WRITE (IOUT,501) IN
  501 FORMAT (/,' OBSERVATION PACKAGE,  INPUT READ FROM UNIT ',I3)
C
C-------READ & PRINT ITEM 1 OF THE HOB INPUT FILE
      CALL URDCOM(IUMISC,LINE,IULOC)
      LLOC = 1
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,0,IDUM,DUM,IOUT,IN)
      OUTNAM=LINE(ISTART:ISTOP)
      WRITE(IOUT,*) 'OUTNAM: ',LINE(ISTART:ISTOP)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,ISCALS,DUM,IOUT,IN)
      IF(ISCALS.LT.0) ISCALS=-1
      IF(ISCALS.GT.3) ISCALS=-1
      WRITE(IOUT,*) 'ISCALS=',ISCALS
      CALL IVLSAV('ISCALS',0,0,ISCALS)
      CLOSE(UNIT=IUMISC)
      RETURN
      END
      SUBROUTINE HOBIN()
C     ******************************************************************
C     Read Head Observations -- MF2K version
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      COMMON /SHOBCOM/LAYSHOB(10,MXHOB),IROWSHOB(MXHOB),ICOLSHOB(MXHOB),
     1               SHROFF(MXHOB),SHCOFF(MXHOB),SHOBSTAT(MXHOB),
     2               SHOBS(MXHOB),ISHOBST(MXHOB),ISHREFSP(MXHOB),
     3               SHOBTOFF(MXHOB),ISHOBPLT(MXHOB),SHOBPR(10,MXHOB)
      COMMON /CSHOBCOM/SHOBNAM
      CHARACTER*12 SHOBNAM(MXHOB)
      COMMON /HOBCOM/LAYHOB(10,MXHOB),IROWHOB(MXHOB),ICOLHOB(MXHOB),
     1               ROFF(MXHOB),COFF(MXHOB),HOBSTAT(MXHOB),HOBS(MXHOB),
     2               IHOBST(MXHOB),IREFSP(MXHOB),HOBTOFF(MXHOB),
     3               IHOBPLT(MXHOB),ITT(MXHOB),DDSTAT(MXHOB),
     4               HOBPR(10,MXHOB)
      COMMON /CHOBCOM/HOBNAM
      CHARACTER*12 HOBNAM(MXHOB),HNAM
      CHARACTER*200 LINE
C     ------------------------------------------------------------------
C
      IULOC=28
      IN=IUNIT(IULOC)
      OPEN(UNIT=IUMISC,FILE=FILNAM(IN))

      DO 10 I=1,MXHOB
      SHOBNAM(I)=' '
      IROWSHOB(I)=0
      ICOLSHOB(I)=0
      SHROFF(I)=0.
      SHCOFF(I)=0.
      SHOBSTAT(I)=1
      SHOBS(I)=0.
      ISHOBST(I)=1
      ISHREFSP(I)=1
      SHOBTOFF(I)=0.
      ISHOBPLT(I)=1
      HOBNAM(I)=' '
      IROWHOB(I)=0
      ICOLHOB(I)=0
      ROFF(I)=0.
      COFF(I)=0.
      HOBSTAT(I)=1
      HOBS(I)=0.
      IHOBST(I)=1
      IREFSP(I)=1
      HOBTOFF(I)=0.
      IHOBPLT(I)=1
      ITT(I)=0
      DDSTAT(I)=0.
      DO 9 J=1,10
      SHOBPR(J,I)=0.
      LAYSHOB(J,I)=0
      HOBPR(J,I)=0.
      LAYHOB(J,I)=0
9     CONTINUE
10    CONTINUE
C
C1------IDENTIFY PROCESS AND PRINT HEADING
      WRITE (IOUT,501) IN
  501 FORMAT (/,' HOB -- HEAD OBSERVATION PACKAGE, ',
     &        ' INPUT READ FROM UNIT ',I3)
C
C-------READ & PRINT ITEM 1 OF THE HOB INPUT FILE
      CALL URDCOM(IUMISC,LINE,IULOC)
      LLOC = 1
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,NH,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,MOBS,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,MAXM,DUM,IOUT,IN)
      WRITE (IOUT,505) NH
  505 FORMAT (/,
     &   ' NUMBER OF HEAD OBSERVATIONS:',I5)
C
C
C     READ ITEM 2: CONTROLS ON PRINTING/SAVING OF SENSITIVITY ARRAYS
      READ (IUMISC,*) TOMULTH,EVH
      CALL VALSAV('TOMULTH',0,0,TOMULTH)
      CALL VALSAV('EVH',0,0,EVH)
C
C  Read head observations
C  I is the number of observations as counted by MODFLOW
C  NHOB is the number of multi-time entries in the HOB list
C  NSHOB is the number of single-time observations in the SHOB list
      I=0
      NHOB=0
      NSHOB=0
11    I=I+1
C
      READ(IUMISC,*) HNAM,LAYH,IROWH,ICOLH,IRSPH,TOFFH,ROFFH,
     1     COFFH,H,STATH,ISTH,IPLTH
      CALL UTL_CHECK_NAMES(IOUT,HNAM)
      IF(ISTH.LT.0 .OR. ISTH.GT.2) ISTH=0
C
      IF(IRSPH.GE.0) THEN
C  Single-time
         NSHOB=NSHOB+1
         IF(NSHOB.GT.MXHOB) THEN
            WRITE(IOUT,*) MXHOB,
     1' is the max. number of single-time HOBS supported by the program'
            CALL RDABRT
         END IF
C
         DO 20 J=1,10
         LAYSHOB(J,NSHOB)=0
         SHOBPR(J,NSHOB)=0.
20       CONTINUE
         SHOBNAM(NSHOB)=HNAM
         LAYSHOB(1,NSHOB)=LAYH
         IROWSHOB(NSHOB)=IROWH
         ICOLSHOB(NSHOB)=ICOLH
         ISHREFSP(NSHOB)=IRSPH
         SHOBTOFF(NSHOB)=TOFFH
         SHROFF(NSHOB)=ROFFH
         SHCOFF(NSHOB)=COFFH
         SHOBS(NSHOB)=H
         SHOBSTAT(NSHOB)=STATH
C  ucode uses different stat-flag codes
         ISHOBST(NSHOB)=ISTH+1
         ISHOBPLT(NSHOB)=IPLTH
         WRITE(IOUT,*) SHOBNAM(NSHOB),LAYSHOB(1,NSHOB),IROWSHOB(NSHOB),
     1           ICOLSHOB(NSHOB),ISHREFSP(NSHOB),SHOBTOFF(NSHOB),
     2           SHROFF(NSHOB),SHCOFF(NSHOB),SHOBS(NSHOB),
     3           SHOBSTAT(NSHOB),ISHOBST(NSHOB),ISHOBPLT(NSHOB)
C
C  Read layers if multi-layer
         IF(LAYSHOB(1,NSHOB).LT.0) THEN
            NL=-LAYSHOB(1,NSHOB)
            IF(NL.GT.10) THEN
               WRITE(IOUT,*)
     1      ' This program limits multi-layer HOBS to 10 layers.'
               CALL RDABRT
            END IF
            READ(IUMISC,*) (LAYSHOB(J,NSHOB),SHOBPR(J,NSHOB),J=1,NL)
         END IF
C
      ELSE
C  Read times if multi-times
         NHOB=NHOB+1
         IF(NHOB.GT.MXHOB) THEN
            WRITE(IOUT,*) MXHOB,
     1' is the max. number of multi-time HOBS supported by this program'
            CALL RDABRT
         END IF
C
         DO 25 J=1,10
         LAYHOB(J,NHOB)=0
         HOBPR(J,NHOB)=0.
25       CONTINUE
C
         HOBNAM(NHOB)=HNAM
         LAYHOB(1,NHOB)=LAYH
         IROWHOB(NHOB)=IROWH
         ICOLHOB(NHOB)=ICOLH
         IREFSP(NHOB)=IRSPH
         HOBTOFF(NHOB)=TOFFH
         ROFF(NHOB)=ROFFH
         COFF(NHOB)=COFFH
         HOBS(NHOB)=H
         HOBSTAT(NHOB)=STATH
C  ucode uses different stat-flag codes
         IHOBST(NHOB)=ISTH+1
         IHOBPLT(NHOB)=IPLTH
         WRITE(IOUT,*) HOBNAM(NHOB),LAYHOB(1,NHOB),IROWHOB(NHOB),
     1           ICOLHOB(NHOB),IREFSP(NHOB),
     2           HOBSTAT(NHOB),IHOBST(NHOB),IHOBPLT(NHOB)
C
C  Read layers if multi-layer
         IF(LAYHOB(1,NHOB).LT.0) THEN
            NL=-LAYHOB(1,NHOB)
            IF(NL.GT.10) THEN
               WRITE(IOUT,*)
     1   ' This program limits multi-layer HOBS to 10 layers.'
               CALL RDABRT
            END IF
            READ(IUMISC,*) (LAYHOB(J,NHOB),HOBPR(J,NHOB),J=1,NL)
         END IF
C
C  Set ITT=1 for multitime header with head as the observation
C  Set ITT=2 for multitime header with drawdown as the observation
C  Set ITT=3 for a multitime item 6 record
         NT=-IREFSP(NHOB)
         READ(IUMISC,*) ITT(NHOB)
         IF(ITT(NHOB).NE.2) ITT(NHOB)=1
         IDD=ITT(NHOB)
         DO 60 J=1,NT
         NHOB=NHOB+1
         IF(J.NE.1) I=I+1
         IF(NHOB.GT.MXHOB) THEN
            WRITE(IOUT,*) MXHOB,
     1      ' is the maximum number of HOBS supported by this program'
            CALL RDABRT
         END IF
         READ(IUMISC,*) HOBNAM(NHOB),IREFSP(NHOB),HOBTOFF(NHOB),
     1             HOBS(NHOB),HOBSTAT(NHOB),DDSTAT(NHOB),ISTH,
     2             IHOBPLT(NHOB)
         CALL UTL_CHECK_NAMES(IOUT,HOBNAM(NHOB))
         IF(ISTH.LT.0 .OR. ISTH.GT.2) ISTH=0
         IF(J.NE.1 .AND. IDD.EQ.2) HOBSTAT(NHOB)=DDSTAT(NHOB)
C  ucode uses different stat-flag codes
         IHOBST(NHOB)=ISTH+1
         IROWHOB(NHOB)=0
         ICOLHOB(NHOB)=0
         ITT(NHOB)=3
         WRITE(IOUT,*) HOBNAM(NHOB),IREFSP(NHOB),HOBTOFF(NHOB),
     1         HOBS(NHOB),HOBSTAT(NHOB),IHOBST(NHOB),IHOBPLT(NHOB)
60       CONTINUE
      END IF
C
      IF(I.LT.NH) GO TO 11
C
      CLOSE(UNIT=IUMISC)
      RETURN
      END
      SUBROUTINE SENIN()
C     ******************************************************************
C     Read Sensitivity file
C     ******************************************************************
C     SPECIFICATIONS:
C     ------------------------------------------------------------------
      INCLUDE 'mfi2k.inc'
      INCLUDE 'mfi2kpar.inc'
      CHARACTER*200 LINE
      CHARACTER*10 PN,PNI,PNUL
      CHARACTER*18 FTYP
C     ------------------------------------------------------------------
      IULOC=25
      IN=IUNIT(IULOC)
      OPEN(UNIT=IUMISC,FILE=FILNAM(IN))
C
C1------IDENTIFY PROCESS AND PRINT HEADING
      WRITE (IOUT,501) IN
  501 FORMAT (/,' SENBAS1 -- SENSITIVITY PROCESS, ',
     &        'VERSION 1.0, 10/15/98',/,' INPUT READ FROM UNIT ',I3)
C
C-------READ & PRINT ITEM 1 OF THE SEN INPUT FILE
      CALL URDCOM(IUMISC,LINE,IULOC)
      LLOC = 1
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,NPLIST,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,ISENALL,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IUHEAD,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,MXSEN,DUM,IOUT,IN)
      CALL IVLSAV('ISENAL',0,0,ISENALL)
      CALL IVLSAV('IUHEAD',0,0,IUHEAD)
      WRITE (IOUT,505) NPLIST
  505 FORMAT (/,
     &   ' NUMBER OF PARAMETER VALUES TO BE READ FROM SEN FILE:',I5)
C
      WRITE (IOUT,560) ISENALL
  560 FORMAT (' ISENALL............................................:',
     &        I5)
C
      IF (IUHEAD.GT.0) THEN
        WRITE (IOUT,600)
  600   FORMAT(' SENSITIVITIES WILL BE STORED IN SCRATCH FILES')
      ELSE
        WRITE (IOUT,620)
  620   FORMAT(' SENSITIVITIES WILL BE STORED IN MEMORY')
      ENDIF
      WRITE (IOUT,630) MXSEN
  630 FORMAT(' FOR UP TO ',I3,' PARAMETERS')
C
C     READ ITEM 2: CONTROLS ON PRINTING/SAVING OF SENSITIVITY ARRAYS
      READ (IUMISC,*) IPRINTS, ISENSU, ISENPU, ISENFM
      IF(IPRINTS.NE.0 .AND. IPRINTS.NE.1) IPRINTS=0
      CALL IVLSAV('IPRNTS',0,0,IPRINTS)
      CALL IVLSAV('ISENFM',0,0,ISENFM)
      CALL IVLSAV('ISENSU',0,0,ISENSU)
      CALL IVLSAV('ISENPU',0,0,ISENPU)
C
      IF (ISENSU.GT.0 .OR. ISENPU.GT.0) THEN
        IF (IPRINTS.EQ.0) THEN
          WRITE (IOUT,730)
  730 FORMAT (1X,'OUTPUT CONTROL FILE WILL CONTROL PRINTING/SAVING',
     &' OF SENSITIVITY ARRAYS')
        ELSEIF (IPRINTS.EQ.1) THEN
          WRITE (IOUT,740)
  740 FORMAT (1X,'SENSITIVITY ARRAYS WILL BE PRINTED AND/OR SAVED FOR',
     &' ALL TIME STEPS')
        ENDIF
      ELSE
        WRITE (IOUT,660)
  660 FORMAT (1X,'SENSITIVITY ARRAYS WILL NOT BE PRINTED OR SAVED')
      ENDIF
C
          IF (ISENSU.GT.0) WRITE (IOUT,760) ISENSU
  760 FORMAT(1X,'BINARY SENSITIVITY ARRAYS WILL BE SAVED TO UNIT ',I4)
          IF (ISENPU.GT.0) WRITE (IOUT,770) ISENPU, ISENFM
  770 FORMAT(1X,'SENSITIVITY ARRAYS WILL BE PRINTED TO UNIT ',I4,
     &       ' USING FORMAT CODE ',I3)
  850 FORMAT(/,1X,57('*'),/,
     &       6X,'USE OF THE ',A3,' PACKAGE IS INCOMPATIBLE WITH THE',/,
     &       6X,'SENSITIVITY PROCESS',/,
     &       6X,'-- STOP EXECUTION',/,1X,57('*'))
C
C-----READ ITEM 3:  LIST OF PARAMETER INFORMATION
      IF (NPLIST.GT.0) THEN
        WRITE (IOUT,520)
 520  FORMAT (/,' INFORMATION ON PARAMETERS LISTED IN SEN FILE',/,
     &   43X,'LOWER',9X,'UPPER',/
     &   26X,'VALUE IN SEN   REASONABLE    REASONABLE',/,
     &   '    NAME      ISENS   LN   INPUT FILE      LIMIT  ',
     &   '       LIMIT',/,
     &   ' ----------  -------  --  ------------  ------------',
     &   '  ------------')
        DO 500 N=1,NPLIST
          READ(IUMISC,*) PN,ISENSTMP,LNTMP,BTMP,BLTMP,BUTMP,BSTMP
          WRITE(IOUT,570) PN,ISENSTMP,LNTMP,BTMP,BLTMP,BUTMP,BSTMP
 570      FORMAT(1X,A10,4X,I2,5X,I2,4(2X,G12.5))
          IF(ISENSTMP.NE.0) ISENSTMP=1
C
C---------Find parameter in list of parameters
          PNUL=PN
          CALL UPCASE(PN)
          IF(IPSUM.GT.0) THEN
            DO 100 I=1,IPSUM
            PNI=PARNAM(I)
            CALL UPCASE(PNI)
            IF(PNI.EQ.PN) THEN
               IF(ISFILE(I).EQ.1) THEN
                  WRITE(IOUT,*) 'Duplicate entry in Sensitivity file'
                  CALL RDABRT
               END IF
               GO TO 200
            ENDIF
100         CONTINUE
          ENDIF
          IPSUM=IPSUM+1
          I=IPSUM
          PARNAM(I)=PNUL
C
200       ISFILE(I)=1
          ISENS(I)=ISENSTMP
          LN(I)=LNTMP
          B2(I)=BTMP
          BL(I)=BLTMP
          BU(I)=BUTMP
          BSCAL(I)=BSTMP
          BSTART(I)=B2(I)
          ISTRAIN(I)=0
          VALLOW(I)=BL(I)
          VALUP(I)=BU(I)
C
500     CONTINUE
      ENDIF
C
      CLOSE(UNIT=IUMISC)
      RETURN
      END
      SUBROUTINE PESIN()
C     ******************************************************************
C     Read PES file -- MF2K
C     ******************************************************************
C     SPECIFICATIONS:
C     ------------------------------------------------------------------
      INCLUDE 'mfi2k.inc'
      INCLUDE 'mfi2kpar.inc'
      COMMON /PESCOM/PRM(200),PRMSP(200),IPRMSF(200),IPRMPS(200)
      COMMON /CPESCOM/PARNEG(200),EQNAM(200),PESEQ(200)
      CHARACTER*10 PARNEG,EQNAM
      CHARACTER*100 PESEQ
      CHARACTER*200 LINE
      CHARACTER*10 CTMP1,CTMP2
C     ------------------------------------------------------------------
      IULOC=26
      IN=IUNIT(IULOC)
      OPEN(UNIT=IUMISC,FILE=FILNAM(IN))
C
C1------IDENTIFY PROCESS AND PRINT HEADING
      WRITE (IOUT,501) IN
  501 FORMAT (/,' PESBAS1 -- PARAMETER ESTIMATION PROCESS, ',
     &        'VERSION 1.0, 10/15/98',/,' INPUT READ FROM UNIT ',I3)
C
C-------READ & PRINT ITEM 1 OF THE PES INPUT FILE
      CALL URDCOM(IUMISC,LINE,IULOC)
      LLOC = 1
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,ITMXP,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,3,IDUM,DMAX,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,3,IDUM,TOL,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,3,IDUM,SOSC,IOUT,IN)
      CALL IVLSAV('ITMXP',0,0,ITMXP)
      CALL VALSAV('DMAX',0,0,DMAX)
      CALL VALSAV('TOL',0,0,TOL)
      CALL VALSAV('SOSC',0,0,SOSC)
      WRITE(IOUT,510) ITMXP,DMAX,TOL,SOSC
  510 FORMAT (/,
     &' MAXIMUM NUMBER OF PARAMETER-ESTIMATION ITERATIONS (MAX-ITER)  ='
     &,1X,I5,/,
     &' MAXIMUM PARAMETER CORRECTION (MAX-CHANGE) ------------------- ='
     &,1X,G11.5,/,
     &' CLOSURE CRITERION (TOL) ------------------------------------- ='
     &,1X,G11.5,/,
     &' SUM OF SQUARES CLOSURE CRITERION (SOSC) --------------------- ='
     &,1X,G11.5)
C
C     READ AND PRINT ITEM 2 OF THE PES INPUT FILE
      READ(IUMISC,'(A)') LINE
      LLOC = 1
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IBEFLG,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IYCFLG,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IOSTAR,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,NOPT,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,NFIT,DUM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,3,IDUM,SOSR,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,3,IDUM,RMAR,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,3,IDUM,RMARM,IOUT,IN)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IAP,DUM,IOUT,IN)
      IF(IAP.NE.1) IAP=0
      CALL IVLSAV('IBEFLG',0,0,IBEFLG)
      CALL IVLSAV('IYCFLG',0,0,IYCFLG)
      CALL IVLSAV('IOSTAR',0,0,IOSTAR)
      CALL IVLSAV('NOPT',0,0,NOPT)
      CALL IVLSAV('NFIT',0,0,NFIT)
      CALL VALSAV('SOSR',0,0,SOSR)
      CALL VALSAV('RMAR',0,0,RMAR)
      CALL VALSAV('RMARM',0,0,RMARM)
      CALL IVLSAV('IAP',0,0,IAP)
      WRITE(IOUT,520) IBEFLG,IYCFLG,IOSTAR,NOPT,NFIT,SOSR,RMAR,RMARM,IAP
  520 FORMAT (/,
     &' FLAG TO GENERATE INPUT NEEDED BY BEALE-2000 (IBEFLG) -------- ='
     &,1X,I5,/,
     &' FLAG TO GENERATE INPUT NEEDED BY YCINT-2000 (IYCFLG) -------- ='
     &,1X,I5,/,
     &' OMIT PRINTING TO SCREEN (IF = 1) (IOSTAR) ------------------- ='
     &,1X,I5,/,
     &' ADJUST GAUSS-NEWTON MATRIX WITH NEWTON UPDATES (IF = 1)(NOPT) ='
     &,1X,I5,/,
     &' NUMBER OF FLETCHER-REEVES ITERATIONS (NFIT) ----------------- ='
     &,1X,I5,/,
     &' CRITERION FOR ADDING MATRIX R (SOSR) ------------------------ ='
     &,1X,G11.5,/,
     &' MARQUARDT PARAMETER (RMAR) ---------------------------------- ='
     &,1X,G11.5,/,
     &' MARQUARDT MULTIPLIER (RMARM) -------------------------------- ='
     &,1X,G11.5,/,
     &' APPLY MAX-CHANGE IN REGRESSION SPACE (IF = 1) (IAP) --------- ='
     &,1X,I5)
C
      IF (IBEFLG.LT.0 .OR. IBEFLG.GT.2) THEN
        WRITE (IOUT,521)
  521 FORMAT (/,1X,'ERROR: IBEFLG MUST BE 0, 1, OR 2 -- STOP',
     &' EXECUTION (PESBAS1AL)')
        CALL RDABRT
      ENDIF
C
      IF (IYCFLG.LT.-1 .OR. IYCFLG.GT.2) THEN
        WRITE (IOUT,522)
  522 FORMAT (/,1X,'ERROR: IYCFLG MUST BE -1, 0, 1, OR 2 -- STOP',
     &' EXECUTION (PESBAS1AL)')
        CALL RDABRT
      ENDIF
C
C
C     READ AND PRINT ITEM 3 OF THE PES INPUT FILE
      READ(IUMISC,'(A)') LINE
      LLOC = 1
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IPRC,DUM,IOUT,IU)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IPRINT,DUM,IOUT,IU)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,LPRINT,DUM,IOUT,IU)
      CALL IVLSAV('IPRC',0,0,IPRC)
      CALL IVLSAV('IPRINT',0,0,IPRINT)
      CALL IVLSAV('LPRINT',0,0,LPRINT)
      WRITE (IOUT,525) IPRC, IPRINT, LPRINT
  525 FORMAT (/,
     &' FORMAT CODE FOR COVARIANCE AND CORRELATION MATRICES (IPRCOV)  ='
     &,1X,I5,/,
     &' PRINT PARAMETER-ESTIMATION STATISTICS',/,
     &'     EACH ITERATION (IF > 0)  (IPRINT) ----------------------- ='
     &,1X,I5,/,
     &' PRINT EIGENVALUES AND EIGENVECTORS OF',/,
     &'     COVARIANCE MATRIX (IF > 0)  (LPRINT) -------------------- ='
     &,1X,I5)
C
C     READ AND PRINT ITEM 4 OF THE PES INPUT FILE
      READ(IUMISC,'(A)') LINE
      LLOC = 1
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,3,IDUM,CSA,IOUT,IU)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,3,IDUM,FCONV,IOUT,IU)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,LASTX,DUM,IOUT,IU)
      CALL VALSAV('CSA',0,0,CSA)
      CALL VALSAV('FCONV',0,0,FCONV)
      CALL IVLSAV('LASTX',0,0,LASTX)
      WRITE (IOUT,530) CSA, FCONV, LASTX
  530 FORMAT (/,
     &' SEARCH DIRECTION ADJUSTMENT PARAMETER (CSA) ----------------- ='
     &,1X,G11.5,/,
     &' MODIFY CONVERGENCE CRITERIA (IF > 0) (FCONV) ---------------- ='
     &,1X,G11.5,/,
     &' CALCULATE SENSITIVITIES USING FINAL',/,
     &'     PARAMETER ESTIMATES (IF > 0) (LASTX) -------------------- ='
     &,1X,I5)
C
C     READ AND PRINT ITEM 5 OF THE PES INPUT FILE
      READ(IUMISC,'(A)') LINE
      LLOC = 1
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,NPNG,DUM,IOUT,IU)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IFPR,DUM,IOUT,IU)
      CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,MPR,DUM,IOUT,IU)
      CALL IVLSAV('MPR',0,0,MPR)
      WRITE(IOUT,535) NPNG,IFPR,MPR
  535 FORMAT (/,
     &' NUMBER OF USUALLY POS. PARAMETERS THAT MAY BE NEG (NPNG) ---- ='
     &,1X,I5,/,
     &' NUMBER OF PARAMETERS WITH CORRELATED PRIOR INFORMATION (IFPR) ='
     &,1X,I5,/,
     &' NUMBER OF PRIOR-INFORMATION EQUATIONS (MPR) ----------------- ='
     &,1X,I5)
      IF(NPNG.GT.200) THEN
         WRITE(IOUT,*)
     1         ' NPNG MUST BE 200 OR LESS IN THIS CONVERSION PROGRAM'
         CALL RDABRT
      END IF
C
C-------READ ITEM 6
      IF (NPNG.GT.0) THEN
        READ(IUMISC,*) (PARNEG(I),I=1,NPNG)
        DO 8 I = 1,NPNG
          CTMP1=PARNEG(I)
          CALL UPCASE(CTMP1)
          DO 4 IP = 1,IPSUM
            CTMP2=PARNAM(IP)
            CALL UPCASE(CTMP2)
            IF(CTMP1.EQ.CTMP2) GOTO 6
  4       CONTINUE
          WRITE (IOUT,640) PARNEG(I)
  640 FORMAT (/,' ERROR: PARAMETER NAME "',A,'" NOT DEFINED',/,
     &          ' -- STOP EXECUTION (PESBAS1RP)')
          CALL RDABRT
  6       CONTINUE
  8     CONTINUE
        WRITE (IOUT,541)
  541   FORMAT (/,
     &' PARAMETERS FOR WHICH PARTYP = HK, VK, VANI, VKCB, SS, SY,',
     &' OR EVT,',/,' BUT FOR WHICH PARAMETERS CAN BE NEGATIVE:')
        WRITE (IOUT,542) (PARNEG(I),I=1,NPNG)
  542   FORMAT(3X,A)
      ENDIF
C
      IF(MPR.GT.200) THEN
         WRITE(IOUT,*)
     1         'MPR must be 200 or less in this conversion program'
         CALL RDABRT
      END IF
      IF(IFPR.GT.0) THEN
         WRITE(IOUT,*) 'IFPR must be 0 in this conversion program'
         CALL RDABRT
      END IF
C
      IF(MPR.GT.0) THEN
         DO 700 I=1,MPR
         READ(IUMISC,'(A)') LINE
         WRITE(IOUT,*) LINE
         LLOC=1
         CALL URWORD(LINE,LLOC,ISTART,ISTOP,0,IDUM,RDUM,IOUT,IN)
         EQNAM(I)=LINE(ISTART:ISTOP)
         CALL UTL_CHECK_NAMES(IOUT,EQNAM(I))
         CALL URWORD(LINE,LLOC,ISTART,ISTOP,3,IDUM,PRM(I),IOUT,IN)
         CALL URWORD(LINE,LLOC,ISTART,ISTOP,0,IDUM,RDUM,IOUT,IN)
         IF(LINE(ISTART:ISTOP).NE.'=') THEN
            WRITE(IOUT,*)
     1      '"=" not found in required location in prior equation'
            CALL RDABRT
         END IF
         CALL URWORD(LINE,LLOC,ISTART,ISTOP,1,IDUM,RDUM,IOUT,IN)
         IEQBEG=ISTART
690      CALL URWORD(LINE,LLOC,ISTART,ISTOP,1,IDUM,RDUM,IOUT,IN)
         IF(LINE(ISTART:ISTOP).EQ.' ') THEN
            WRITE(IOUT,*)
     1      '"STAT" not found in required location in prior equation'
            CALL RDABRT
         END IF
         IF(LINE(ISTART:ISTOP).NE.'STAT') GO TO 690
         PESEQ(I)=LINE(IEQBEG:ISTOP-5)
         CALL URWORD(LINE,LLOC,ISTART,ISTOP,3,IDUM,PRMSP(I),IOUT,IN)
         CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IPRMSF(I),RDUM,IOUT,IN)
         IF(IPRMSF(I).LT.0 .OR. (IPRMSF(I).GT.12) .OR.
     1       (IPRMSF(I).GT.2.AND.IPRMSF(I).LT.10) ) THEN
            WRITE(IOUT,*)
     1      'Illegal STAT-FLAG'
            CALL RDABRT
         END IF
         IF(IPRMSF(I).GT.2) IPRMSF(I)=IPRMSF(I)-10
         IPRMSF(I)=IPRMSF(I)+1
         CALL URWORD(LINE,LLOC,ISTART,ISTOP,2,IPRMPS(I),RDUM,IOUT,IN)
700      CONTINUE
      END IF
C
      CLOSE(UNIT=IUMISC)
      RETURN
      END
      SUBROUTINE UTL_CHECK_NAMES(IOUT,NAME)
C   Modified from Jupiter API subroutine of the same name published in:
C   Banta, E.R., Poeter, E.P., Doherty, J.E., and Hill, M.C., 2006,
C   "JUPITER: Joint Universal Parameter IdenTification and Evaluation
C    of Reliability-- An application programming interface (API) for
C    model analysis": U.S. Geological Survey Techniques and Methods
C   Book 6, Section E, Chapter 1, 268 p.
C   Left-justify the name, then check each the name for conformance
C   with the JUPITER naming convention:
C   (1) The first character must be a letter; and
C   (2) All characters after the first letter must be a letter, digit,
C       or member of the set: "_", ".", ":", "&", "#", "@"
C       (underscore, dot, colon, ampersand, number sign, at symbol).
C
C   Argument-list variables
      INTEGER,                           INTENT(IN)    :: IOUT
      CHARACTER(LEN=*)                 , INTENT(INOUT) :: NAME
C
C   Local variables
      INTEGER :: I, ICONV, ILOC, L, LENNAME
      CHARACTER*20 NAMTMP
      CHARACTER(LEN=68) :: LEGAL
      DATA LEGAL 
C                  1         2         3         4         5         6         7
C         1234567890123456789012345678901234567890123456789012345678901234567890
     1  /'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789
     2_.:&#@'/
C
C
CICONV = 63  ! Fortran naming convention
      ICONV = 68  ! Jupiter naming convention
C
C  Check only non-blank names
      IF (NAME.NE.' ') THEN
C  Left justify
        NAME = ADJUSTL(NAME)
        NAMTMP=NAME
        LENNAME = LEN_TRIM(NAME)
C  Check 1st character -- must be alphabetic
        ILOC = INDEX(LEGAL(1:52),NAME(1:1))
        IF (ILOC==0) THEN
C  Convert bad character to an uppercase alphabetic character
          IBAD=ICHAR(NAME(1:1))
C Add an offset so that a numeric 0 gets converted to "A"
C  "0" is character 48, and adding 4 will produce 52, which will be
C   converted to 1 by the MOD function.  LEGAL(1)="A"
          IBAD=IBAD+4
          IGOOD=MOD(IBAD,26)+1
          NAME(1:1)=LEGAL(IGOOD:IGOOD)
        ENDIF
C  Check remaining characters for any of the valid characters
        IF (LENNAME>1) THEN
          DO L=2,LENNAME
          ILOC = INDEX(LEGAL(1:ICONV),NAME(L:L))
            IF (ILOC==0) THEN
C  Convert bad character to an uppercase  alphabetic character
              IBAD=ICHAR(NAME(L:L))
C Add an offset so that "-" gets converted to "_"
C  "-" is character 45, and adding 17 will produce 62, which will be
C   converted to 63 by the MOD function.  LEGAL(63)="_"
              IBAD=IBAD+17
              IGOOD=MOD(IBAD,68)+1
              NAME(L:L)=LEGAL(IGOOD:IGOOD)
            ENDIF
          ENDDO
        ENDIF
        IF(NAME.NE.NAMTMP)
     1      WRITE(IOUT,*) TRIM(NAMTMP),' changed to ',TRIM(NAME)
      ENDIF
C
      RETURN
C
      END SUBROUTINE UTL_CHECK_NAMES
      SUBROUTINE DUPOBS
C     ******************************************************************
C  Make all observation names unique
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      COMMON /FOBCOM/LAYFOB(MXCFOB),IROWFOB(MXCFOB),ICOLFOB(MXCFOB),
     1               FOBFACT(MXCFOB),FOBSTAT(MXTFOB),FOBS(MXTFOB),
     2               IFOBST(MXTFOB),IFOBRSP(MXTFOB),FOBTOFF(MXTFOB),
     3               IFOBPLT(MXTFOB),NGFOB,NTFOB,NCFOB,NFOB(6,MXGFOB),
     4               FOBCON(2,40)
      COMMON /CFOBCOM/FOBNAM
      CHARACTER*12 FOBNAM(MXTFOB)      
      COMMON /SHOBCOM/LAYSHOB(10,MXHOB),IROWSHOB(MXHOB),ICOLSHOB(MXHOB),
     1               SHROFF(MXHOB),SHCOFF(MXHOB),SHOBSTAT(MXHOB),
     2               SHOBS(MXHOB),ISHOBST(MXHOB),ISHREFSP(MXHOB),
     3               SHOBTOFF(MXHOB),ISHOBPLT(MXHOB),SHOBPR(10,MXHOB)
      COMMON /CSHOBCOM/SHOBNAM
      CHARACTER*12 SHOBNAM(MXHOB)
      COMMON /HOBCOM/LAYHOB(10,MXHOB),IROWHOB(MXHOB),ICOLHOB(MXHOB),
     1               ROFF(MXHOB),COFF(MXHOB),HOBSTAT(MXHOB),HOBS(MXHOB),
     2               IHOBST(MXHOB),IREFSP(MXHOB),HOBTOFF(MXHOB),
     3               IHOBPLT(MXHOB),ITT(MXHOB),DDSTAT(MXHOB),
     4               HOBPR(10,MXHOB)
      COMMON /CHOBCOM/HOBNAM
      CHARACTER*12 HOBNAM(MXHOB),HNAM
      CHARACTER*12 TNAM,NEWNAM
C     ------------------------------------------------------------------
      WRITE(*,*) 'Checking for duplicate observation names...'
C
C  Check Flow obs names
      DO 60 I=1,MXTFOB
      IF(FOBNAM(I).EQ.' ') GO TO 62
         TNAM=FOBNAM(I)
         CALL DUPNAM(TNAM,IMATCH,I,1,1)
         IF(IMATCH.EQ.1) GO TO 60
         NTRY=1
55       CALL NAMGEN(TNAM,NEWNAM,NTRY)
         CALL DUPNAM(NEWNAM,IMATCH,1,1,1)
         IF(IMATCH.GT.0) THEN
           NTRY=NTRY+1
           IF(NTRY.LT.1297) GO TO 55
           WRITE(IOUT,*) 'Unable to generate unique observation names'
           CALL RDABRT
         END IF
         WRITE(IOUT,*) FOBNAM(I),' changed to ',NEWNAM
         FOBNAM(I)=NEWNAM
60    CONTINUE
C
C  Check Head observation names
C  Single time
62    DO 70 I=1,MXHOB
      IF(SHOBNAM(I).EQ.' ') GO TO 72
         TNAM=SHOBNAM(I)
         CALL DUPNAM(TNAM,IMATCH,0,I,1)
         IF(IMATCH.EQ.1) GO TO 70
         NTRY=1
65       CALL NAMGEN(TNAM,NEWNAM,NTRY)
         CALL DUPNAM(NEWNAM,IMATCH,1,1,1)
         IF(IMATCH.GT.0) THEN
           NTRY=NTRY+1
           IF(NTRY.LT.1297) GO TO 65
           WRITE(IOUT,*) 'Unable to generate unique observation names'
           CALL RDABRT
         END IF
         WRITE(IOUT,*) SHOBNAM(I),' changed to ',NEWNAM
         SHOBNAM(I)=NEWNAM
70    CONTINUE
C
C  Multi-Time
72    DO 80 I=1,MXHOB
      IF(HOBNAM(I).EQ.' ') RETURN
         TNAM=HOBNAM(I)
         CALL DUPNAM(TNAM,IMATCH,0,0,I)
         IF(IMATCH.EQ.1) GO TO 80
         NTRY=1
75       CALL NAMGEN(TNAM,NEWNAM,NTRY)
         CALL DUPNAM(NEWNAM,IMATCH,1,1,1)
         IF(IMATCH.GT.0) THEN
           NTRY=NTRY+1
           IF(NTRY.LT.1297) GO TO 75
           WRITE(IOUT,*) 'Unable to generate unique observation names'
           CALL RDABRT
         END IF
         WRITE(IOUT,*) HOBNAM(I),' changed to ',NEWNAM
         HOBNAM(I)=NEWNAM
80    CONTINUE
C
      RETURN
      END
      SUBROUTINE DUPNAM(TNAM,IMATCH,IFLOW,ISTHOB,IMTHOB)
C     ******************************************************************
C  Check if an observation name is unique
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      COMMON /FOBCOM/LAYFOB(MXCFOB),IROWFOB(MXCFOB),ICOLFOB(MXCFOB),
     1               FOBFACT(MXCFOB),FOBSTAT(MXTFOB),FOBS(MXTFOB),
     2               IFOBST(MXTFOB),IFOBRSP(MXTFOB),FOBTOFF(MXTFOB),
     3               IFOBPLT(MXTFOB),NGFOB,NTFOB,NCFOB,NFOB(6,MXGFOB),
     4               FOBCON(2,40)
      COMMON /CFOBCOM/FOBNAM
      CHARACTER*12 FOBNAM(MXTFOB)      
      COMMON /SHOBCOM/LAYSHOB(10,MXHOB),IROWSHOB(MXHOB),ICOLSHOB(MXHOB),
     1               SHROFF(MXHOB),SHCOFF(MXHOB),SHOBSTAT(MXHOB),
     2               SHOBS(MXHOB),ISHOBST(MXHOB),ISHREFSP(MXHOB),
     3               SHOBTOFF(MXHOB),ISHOBPLT(MXHOB),SHOBPR(10,MXHOB)
      COMMON /CSHOBCOM/SHOBNAM
      CHARACTER*12 SHOBNAM(MXHOB)
      COMMON /HOBCOM/LAYHOB(10,MXHOB),IROWHOB(MXHOB),ICOLHOB(MXHOB),
     1               ROFF(MXHOB),COFF(MXHOB),HOBSTAT(MXHOB),HOBS(MXHOB),
     2               IHOBST(MXHOB),IREFSP(MXHOB),HOBTOFF(MXHOB),
     3               IHOBPLT(MXHOB),ITT(MXHOB),DDSTAT(MXHOB),
     4               HOBPR(10,MXHOB)
      COMMON /CHOBCOM/HOBNAM
      CHARACTER*12 HOBNAM(MXHOB),HNAM
      CHARACTER*(*) TNAM
      CHARACTER*12 UCTNAM,UCNAM
C     ------------------------------------------------------------------
C
      IMATCH=0
      UCTNAM=TNAM
      CALL UPCASE(UCTNAM)
      IF(IFLOW.GT.0) GO TO 59
      IF(ISTHOB.GT.0) GO TO 62
      GO TO 72
C
C  Check Flow obs names
59    DO 60 I=IFLOW,MXTFOB
      IF(FOBNAM(I).EQ.' ') GO TO 62
      UCNAM=FOBNAM(I)
      CALL UPCASE(UCNAM)
      IF(UCNAM.NE.UCTNAM) GO TO 60
         IMATCH=IMATCH+1
         IF(IMATCH.GT.1) RETURN
60    CONTINUE
C
C  Check Head observation names
C  Single time
62    DO 70 I=ISTHOB,MXHOB
      IF(SHOBNAM(I).EQ.' ') GO TO 72
      UCNAM=SHOBNAM(I)
      CALL UPCASE(UCNAM)
      IF(UCNAM.NE.UCTNAM) GO TO 70
         IMATCH=IMATCH+1
         IF(IMATCH.GT.1) RETURN
70    CONTINUE
C
C  Multi-Time
72    DO 80 I=IMTHOB,MXHOB
      IF(HOBNAM(I).EQ.' ') RETURN
      UCNAM=HOBNAM(I)
      CALL UPCASE(UCNAM)
      IF(UCNAM.NE.UCTNAM) GO TO 80
         IMATCH=IMATCH+1
         IF(IMATCH.GT.1) RETURN
80    CONTINUE
C
      RETURN
      END
      SUBROUTINE NAMGEN(TNAM,NEWNAM,ICODE)
C     ******************************************************************
C  Generate a new name from an old name
C     ******************************************************************
      CHARACTER*(*) TNAM,NEWNAM
      CHARACTER(LEN=36) :: LEGAL
      DATA LEGAL/'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'/
      SAVE IND
      DATA IND/0/
C     ------------------------------------------------------------------
C
      IND=IND+1
      IND1=MOD(IND-1,36)+1
      IND2=MOD((IND-1)/36,36)+1
      NC=LEN_TRIM(TNAM)
      IF(NC.LT.11) THEN
        NEWNAM=TNAM(1:NC)//LEGAL(IND2:IND2)//LEGAL(IND1:IND1)
      ELSE
        NEWNAM=TNAM(1:10)//LEGAL(IND2:IND2)//LEGAL(IND1:IND1)
      END IF
C
      RETURN
      END
      SUBROUTINE URWORD(LINE,ICOL,ISTART,ISTOP,NCODE,N,R,IOUT,IN)
C
C
C-----VERSION 1003 05AUG1992 URWORD
C     ******************************************************************
C     ROUTINE TO EXTRACT A WORD FROM A LINE OF TEXT, AND OPTIONALLY
C     CONVERT THE WORD TO A NUMBER.
C        ISTART AND ISTOP WILL BE RETURNED WITH THE STARTING AND
C          ENDING CHARACTER POSITIONS OF THE WORD.
C        THE LAST CHARACTER IN THE LINE IS SET TO BLANK SO THAT IF ANY
C          PROBLEMS OCCUR WITH FINDING A WORD, ISTART AND ISTOP WILL
C          POINT TO THIS BLANK CHARACTER.  THUS, A WORD WILL ALWAYS BE
C          RETURNED UNLESS THERE IS A NUMERIC CONVERSION ERROR.  BE SURE
C          THAT THE LAST CHARACTER IN LINE IS NOT AN IMPORTANT CHARACTER
C          BECAUSE IT WILL ALWAYS BE SET TO BLANK.
C        A WORD STARTS WITH THE FIRST CHARACTER THAT IS NOT A SPACE OR
C          COMMA, AND ENDS WHEN A SUBSEQUENT CHARACTER THAT IS A SPACE
C          OR COMMA.  NOTE THAT THESE PARSING RULES DO NOT TREAT TWO
C          COMMAS SEPARATED BY ONE OR MORE SPACES AS A NULL WORD.
C        FOR A WORD THAT BEGINS WITH "'", THE WORD STARTS WITH THE
C          CHARACTER AFTER THE QUOTE AND ENDS WITH THE CHARACTER
C          PRECEDING A SUBSEQUENT QUOTE.  THUS, A QUOTED WORD CAN
C          INCLUDE SPACES AND COMMAS.  THE QUOTED WORD CANNOT CONTAIN
C          A QUOTE CHARACTER.
C        IF NCODE IS 1, THE WORD IS CONVERTED TO UPPER CASE.
C        IF NCODE IS 2, THE WORD IS CONVERTED TO AN INTEGER.
C        IF NCODE IS 3, THE WORD IS CONVERTED TO A REAL NUMBER.
C        NUMBER CONVERSION ERROR IS WRITTEN TO UNIT IOUT IF IOUT IS
C          POSITIVE; ERROR IS WRITTEN TO DEFAULT OUTPUT IF IOUT IS 0;
C          NO ERROR MESSAGE IS WRITTEN IF IOUT IS NEGATIVE.
C     ******************************************************************
C
C        SPECIFICATIONS:
C     ------------------------------------------------------------------
      CHARACTER*(*) LINE
      CHARACTER*20 RW,STRING
C     ------------------------------------------------------------------
C
C1------Set last char in LINE to blank and set ISTART and ISTOP to point
C1------to this blank as a default situation when no word is found.  If
C1------starting location in LINE is out of bounds, do not look for a
C1------word.
      LINLEN=LEN(LINE)
      LINE(LINLEN:LINLEN)=' '
      ISTART=LINLEN
      ISTOP=LINLEN
      LINLEN=LINLEN-1
      IF(ICOL.LT.1 .OR. ICOL.GT.LINLEN) GO TO 100
C
C2------Find start of word, which is indicated by first character that
C2------is not a blank and not a comma.
      DO 10 I=ICOL,LINLEN
      IF(LINE(I:I).NE.' ' .AND. LINE(I:I).NE.',') GO TO 20
10    CONTINUE
      ICOL=LINLEN+1
      GO TO 100
C
C3------Found start of word.  Look for end.
C3A-----When word is quoted, only a quote can terminate it.
20    IF(LINE(I:I).EQ.'''') THEN
         I=I+1
         IF(I.LE.LINLEN) THEN
            DO 25 J=I,LINLEN
            IF(LINE(J:J).EQ.'''') GO TO 40
25          CONTINUE
         END IF
C
C3B-----When word is not quoted, space or comma will terminate.
      ELSE
         DO 30 J=I,LINLEN
         IF(LINE(J:J).EQ.' ' .OR. LINE(J:J).EQ.',') GO TO 40
30       CONTINUE
      END IF
C
C3C-----End of line without finding end of word; set end of word to
C3C-----end of line.
      J=LINLEN+1
C
C4------Found end of word; set J to point to last character in WORD and
C-------set ICOL to point to location for scanning for another word.
40    ICOL=J+1
      J=J-1
      IF(J.LT.I) GO TO 100
      ISTART=I
      ISTOP=J
C
C5------Convert word to upper case and RETURN if NCODE is 1.
      IF(NCODE.EQ.1) THEN
         IDIFF=ICHAR('a')-ICHAR('A')
         DO 50 K=ISTART,ISTOP
            IF(LINE(K:K).GE.'a' .AND. LINE(K:K).LE.'z')
     1             LINE(K:K)=CHAR(ICHAR(LINE(K:K))-IDIFF)
50       CONTINUE
         RETURN
      END IF
C
C6------Convert word to a number if requested.
100   IF(NCODE.EQ.2 .OR. NCODE.EQ.3) THEN
         RW=' '
         L=20-ISTOP+ISTART
         IF(L.LT.1) GO TO 200
         RW(L:20)=LINE(ISTART:ISTOP)
         IF(NCODE.EQ.2) READ(RW,'(I20)',ERR=200) N
         IF(NCODE.EQ.3) READ(RW,'(F20.0)',ERR=200) R
      END IF
      RETURN
C
C7------Number conversion error.
200   IF(NCODE.EQ.3) THEN
         STRING= 'A REAL NUMBER'
         L=13
      ELSE
         STRING= 'AN INTEGER'
         L=10
      END IF
C
C7A-----If output unit is negative, set last character of string to 'E'.
      IF(IOUT.LT.0) THEN
         N=0
         R=0.
         LINE(LINLEN+1:LINLEN+1)='E'
         RETURN
C
C7B-----If output unit is positive; write a message to output unit.
      ELSE IF(IOUT.GT.0) THEN
         IF(IN.GT.0) THEN
            WRITE(IOUT,201) IN,LINE(ISTART:ISTOP),STRING(1:L),LINE
         ELSE
            WRITE(IOUT,202) LINE(ISTART:ISTOP),STRING(1:L),LINE
         END IF
201      FORMAT(1X,/1X,'FILE UNIT',I4,' : ERROR CONVERTING "',A,
     1       '" TO ',A,' IN LINE:',/1X,A)
202      FORMAT(1X,/1X,'KEYBOARD INPUT : ERROR CONVERTING "',A,
     1       '" TO ',A,' IN LINE:',/1X,A)
C
C7C-----If output unit is 0; write a message to default output.
      ELSE
         IF(IN.GT.0) THEN
            WRITE(*,301) IN,LINE(ISTART:ISTOP),STRING(1:L),LINE
301      FORMAT('FILE UNIT',I4,' : ERROR CONVERTING "',A,
     1       '" TO ',A,' IN LINE:',1X,A)
         ELSE
            WRITE(*,302) LINE(ISTART:ISTOP),STRING(1:L),LINE
302      FORMAT('KEYBOARD INPUT : ERROR CONVERTING "',A,
     1       '" TO ',A,' IN LINE:',1X,A)
         END IF
      END IF
C
C7D-----STOP after writing message.
C
      CALL RDABRT
      END
      SUBROUTINE USTRUC(C,NC)
C     ******************************************************************
C     Convert a string to upper case.
C     ******************************************************************
      CHARACTER*(*) C
C
      IDIFF=ICHAR('a')-ICHAR('A')
      DO 10 I=1,NC
      IF(C(I:I).GE.'a' .AND. C(I:I).LE.'z')
     1        C(I:I)=CHAR(ICHAR(C(I:I))-IDIFF)
10    CONTINUE
      RETURN
      END
      SUBROUTINE URDCOM(IN,LINE,IU)
C
C-----VERSION 02FEB1999 URDCOM
C     ******************************************************************
C     READ COMMENTS FROM A FILE AND PRINT THEM.  RETURN THE FIRST LINE
C     THAT IS NOT A COMMENT
C     ******************************************************************
C
C        SPECIFICATIONS:
C     ------------------------------------------------------------------
      INCLUDE 'mfi2k.inc'
      CHARACTER*(*) LINE
C     ------------------------------------------------------------------
      NCOM=0
   10 READ(IN,'(A)') LINE
      IF(LINE(1:1).NE.'#') RETURN
      NCOM=NCOM+1
      L=LEN(LINE)
      IF(L.GT.79) L=79
      DO 20 I=L,1,-1
      IF(LINE(I:I).NE.' ') GO TO 30
   20 CONTINUE
   30 WRITE(IOUT,'(1X,A)') LINE(1:I)
      IF(NCOM.LE.20) COMENT(NCOM,IU)=LINE(1:I)
      GO TO 10
C
      END
      SUBROUTINE UPCASE(WORD)
C     VERSION 17FEB1999
C     ******************************************************************
C     CONVERT A CHARACTER STRING TO ALL UPPER CASE
C     ******************************************************************
C       SPECIFICATIONS:
C     ------------------------------------------------------------------
      CHARACTER WORD*(*)
C
C
      L = LEN(WORD)
      IDIFF=ICHAR('a')-ICHAR('A')
      DO 10 K=1,L
      IF(WORD(K:K).GE.'a' .AND. WORD(K:K).LE.'z')
     1   WORD(K:K)=CHAR(ICHAR(WORD(K:K))-IDIFF)
10    CONTINUE
C
      RETURN
      END
      SUBROUTINE VALSAV(VNAM,LAYER,IPER,VALUE)
C     ******************************************************************
C  Save a scalar variable's REAL value -- create a new variable if the
C  variable doesn't yet exist.
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      CHARACTER*(*) VNAM
C
      IF(NVAR.GT.0) THEN
         DO 10 I=1,NVAR
         IF(VARNAM(I).EQ.VNAM .AND. IVARDA(1,I).EQ.LAYER .AND.
     1    IVARDA(2,I).EQ.IPER) GO TO 100
10       CONTINUE
      END IF
C
C  Create new entry
      NVAR=NVAR+1
      IF(NVAR.GT.MXVAR) THEN
         WRITE(IOUT,*) ' Variable overflow'
         CALL RDABRT
      END IF
      VARNAM(NVAR)=VNAM
      IVARDA(1,NVAR)=LAYER
      IVARDA(2,NVAR)=IPER
      IVARDA(3,NVAR)=0
      VARVAL(NVAR)=VALUE
      RETURN
C
C  Replace old value
100   VARVAL(I)=VALUE
C
      RETURN
      END
      SUBROUTINE IVLSAV(VNAM,LAYER,IPER,IVALUE)
C     ******************************************************************
C  Save a scalar variable's INTEGER value -- create a new variable if the
C  variable doesn't yet exist.
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      CHARACTER*(*) VNAM
C
      IF(NVAR.GT.0) THEN
         DO 10 I=1,NVAR
         IF(VARNAM(I).EQ.VNAM .AND. IVARDA(1,I).EQ.LAYER .AND.
     1    IVARDA(2,I).EQ.IPER) GO TO 100
10       CONTINUE
      END IF
C
C  Create new entry
      NVAR=NVAR+1
      IF(NVAR.GT.MXVAR) THEN
         WRITE(IOUT,*) ' Variable overflow'
         CALL RDABRT
      END IF
      VARNAM(NVAR)=VNAM
      IVARDA(1,NVAR)=LAYER
      IVARDA(2,NVAR)=IPER
      IVARDA(3,NVAR)=0
      VARVAL(NVAR)=IVALUE
      RETURN
C
C  Replace old value
100   VARVAL(I)=IVALUE
C
      RETURN
      END
      SUBROUTINE VALGET(VNAM,LAYER,IPER,ITYPE,VALUE)
C     ******************************************************************
C     Get the REAL value of a stored variable.
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      CHARACTER*(*) VNAM
C
      IF(NVAR.GT.0) THEN
         DO 10 I=1,NVAR
         IF(VARNAM(I).EQ.VNAM .AND. IVARDA(1,I).EQ.LAYER .AND.
     1    IVARDA(2,I).EQ.IPER) GO TO 100
10       CONTINUE
      END IF
      ITYPE=-1
      RETURN
C
C  Found entry
100   ITYPE=IVARDA(3,I)
      VALUE=VARVAL(I)
      RETURN
C
      END
      SUBROUTINE IVLGET(VNAM,LAYER,IPER,ITYPE,IVALUE)
C     ******************************************************************
C     Get the INTEGER value of a stored variable.
C     ******************************************************************
      INCLUDE 'mfi2k.inc'
      CHARACTER*(*) VNAM
C
      IF(NVAR.GT.0) THEN
         DO 10 I=1,NVAR
         IF(VARNAM(I).EQ.VNAM .AND. IVARDA(1,I).EQ.LAYER .AND.
     1    IVARDA(2,I).EQ.IPER) GO TO 100
10       CONTINUE
      END IF
      ITYPE=-1
      RETURN
C
C  Found entry
100   ITYPE=IVARDA(3,I)
      IVALUE=VARVAL(I)
      RETURN
C
      END
      SUBROUTINE MFIUMK
C     ****************************************************************
C     Initialize the list of real units and the list of available
C     units for MODFLOW
C     ****************************************************************
      INCLUDE 'mfi2k.inc'
C
C  Initialize real units
      DO 7 I=1,MXFNAM
      IUREAL(I)=0
7     CONTINUE
C
      MXUNIT=90
      DO 8 I=1,MXUNIT
      IUAVAI(I)=I
8     CONTINUE
C  Reserve real units as needed --
C  In this case, reserve units 5 and 6, which will be used for INBAS and ILOUT.
      IUAVAI(5)=MXUNIT+1
      IUAVAI(6)=MXUNIT+2
C
      RETURN
      END
      SUBROUTINE RDABRT()
C     ****************************************************************
C     Abort PROGRAM
C     ****************************************************************
      INCLUDE 'mfi2k.inc'
C
      WRITE(*,*)
     1   'This program is aborting because of errors when reading data'
      WRITE(*,11) DSNAME(1:NDSNAM)
   11 FORMAT(1X,'Look at the end of file ',A,'.log for error messages')
      STOP
      END
